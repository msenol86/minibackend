import stats_view
import logging
from rest_framework import status
from django.http.response import HttpResponse
from django import db

logger = logging.getLogger(__name__)


def format_stats_view_wrapper(request, user_id):
    logger.info("caching user started: " + str(user_id))
    api_view_class = stats_view.SMSLogShowView()
    api_view_class._format_stats(user=user_id, addresses=None, ignore_cache=True)
    logger.info("caching user finished: " + str(user_id))
    db.connections.close_all()
    return HttpResponse(status=status.HTTP_200_OK)


def format_stats_view_wrapper_direct(user_id):
    logger.info("caching user started: " + str(user_id))
    api_view_class = stats_view.SMSLogShowView()
    api_view_class._format_stats(user=user_id, addresses=None, ignore_cache=True)
    logger.info("caching user finished: " + str(user_id))
    db.connections.close_all()
