# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division
from .base import AnonymousApiView
from sms_analyzer.exceptions import BaseApiError
from .stats import get_stats, CachedValues
from utils import clear_phone_number, check_password_or_auth_token, convert_date_integer_to_date, convert_date_to_integer, DATETIME_FORMAT, get_user
from sms_analyzer import models
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework.negotiation import BaseContentNegotiation
from rest_framework import status
from datetime import date
from collections import OrderedDict
from io import BytesIO
from django.http import HttpResponse
import logging
import xlsxwriter

logger = logging.getLogger(__name__)


class IgnoreClientContentNegotiation(BaseContentNegotiation):
    def select_parser(self, request, parsers):
        """
        Select the first parser in the `.parser_classes` list.
        """
        return parsers[0]

    def select_renderer(self, request, renderers, format_suffix):
        """
        Select the first renderer in the `.renderer_classes` list.
        """
        return (renderers[0], renderers[0].media_type)


class SMSLogShowView(AnonymousApiView):
    form_class = {'GET': None}
    content_negotiation_class = IgnoreClientContentNegotiation

    def _get_first_and_last_days(self, list_of_history):

        return_dict = {"first_day": "0",
                       "first_day_as_date": date(2016, 8, 12),
                       "last_day": "0",
                       "last_day_as_date": date(2016, 8, 12)}

        if list_of_history:
            list_of_days = sorted([an_element['ts'] for an_element in list_of_history])
            return_dict["first_day"] = list_of_days[0]
            return_dict["first_day_as_date"] = convert_date_integer_to_date(list_of_days[0])
            return_dict["last_day"] = list_of_days[-1]
            return_dict["last_day_as_date"] = convert_date_integer_to_date(list_of_days[-1])
            return return_dict
        else:
            return None

    def _save_contacts_stats_to_db_helper(self, contact_stats, p_json_dict, is_for_me):
        owner = "me" if is_for_me else "them"
        # dict_of_stats structure:
        # {'calls': 5,
        #  'emojis': 2,
        #  'exclamations': 1,
        #  'length': 1,
        #  'links': 1,
        #  'questions': 2,
        #  'repeat': 0.33,
        #  'response': 3.55,
        #  'response_score': 8.5,
        #  'texts': 12}
        dict_of_stats = dict([(a, p_json_dict[a][owner]) for a in p_json_dict if isinstance(p_json_dict[a], dict) and "me" in p_json_dict[a] and "them" in p_json_dict[a]])
        contact_stats.text_messages_count = dict_of_stats['texts']
        contact_stats.emojis_count = dict_of_stats['emojis']
        contact_stats.phone_calls_count = dict_of_stats['calls']
        contact_stats.links_count = dict_of_stats['links']
        contact_stats.exclamations_count = dict_of_stats['exclamations']
        contact_stats.initiation_count = dict_of_stats['initiation_count']
        contact_stats.engagement_count = dict_of_stats['engagement_count']
        contact_stats.question_marks_count = dict_of_stats['questions']
        contact_stats.average_message_length = dict_of_stats['length']
        contact_stats.response_score = dict_of_stats['response_score']
        contact_stats.repeat_percentage = dict_of_stats['repeat']
        contact_stats.pictures_count = dict_of_stats['picture_count']
        contact_stats.days_of_calling = dict_of_stats['days_of_calling']
        contact_stats.days_of_texting = dict_of_stats['days_of_texting']
        contact_stats.days_of_correspondence = dict_of_stats['days_of_correspondence']    
        contact_stats.save()

    def _save_contacts_history_stats_to_db(self, contact, history_summary):
        contact.first_day = history_summary["first_day"]
        contact.first_day_as_date = history_summary["first_day_as_date"]
        contact.last_day = history_summary["last_day"]
        contact.last_day_as_date = history_summary["last_day_as_date"]
        contact.save()

    def _save_contacts_stats_to_db(self, user, data):
        filtered_data = [a_json_dict for a_json_dict in data if a_json_dict['id'] != "all"]
        for tmp_json_dict in filtered_data:
            tmp_contact_number = tmp_json_dict['id']
            if models.Contact.objects.filter(user=user, contact_number=tmp_contact_number).exists():
                tmp_contact = models.Contact.objects.get(user=user, contact_number=tmp_contact_number)
                contact_stats_for_me, is_created = models.ContactStats.objects.get_or_create(contact=tmp_contact,
                                                                                             is_for_me=True)
                contact_stats_for_them, is_created = models.ContactStats.objects.get_or_create(contact=tmp_contact,
                                                                                               is_for_me=False)
                history_summary = self._get_first_and_last_days(tmp_json_dict['history'])
                if history_summary:
                    self._save_contacts_history_stats_to_db(tmp_contact, history_summary)
                self._save_contacts_stats_to_db_helper(contact_stats_for_me, tmp_json_dict, is_for_me=True)
                self._save_contacts_stats_to_db_helper(contact_stats_for_them, tmp_json_dict, is_for_me=False)

    def _save_score_to_contact(self, user, data):
        filtered_data = [a_json_dict for a_json_dict in data if a_json_dict['id'] != "all"]
        for tmp_json_dict in filtered_data:
            tmp_contact_number = tmp_json_dict['id']
            tmp_score = tmp_json_dict['total']
            if models.Contact.objects.filter(user=user, contact_number=tmp_contact_number).exists():
                tmp_contact = models.Contact.objects.get(user=user, contact_number=tmp_contact_number)
                tmp_contact.score = tmp_score
                tmp_contact.save()

    def _save_user_stats_to_db_helper(self, userstatsrecord, p_dict, is_for_me):
        owner = 'me' if is_for_me else 'them'
        userstatsrecord.text_messages_count = p_dict['texts'][owner]
        userstatsrecord.average_message_length = p_dict['length'][owner]
        userstatsrecord.phone_calls_count = p_dict['calls'][owner]
        userstatsrecord.emojis_count = p_dict['emojis'][owner]
        userstatsrecord.links_count = p_dict['links'][owner]
        userstatsrecord.exclamations_count = p_dict['exclamations'][owner]
        userstatsrecord.question_marks_count = p_dict['questions'][owner]
        userstatsrecord.response_score = p_dict['response_score'][owner]
        userstatsrecord.repeat_percentage = p_dict['repeat'][owner]
        userstatsrecord.pictures_count = p_dict['picture_count'][owner]
        userstatsrecord.save()

    def _save_user_stats_to_db(self, user, data):
        # filtered_data:
        # {'calls': {'me': 491.0, 'ratio': 1.3128061581525543, 'them': 938.0},
        #   'emojis': {'me': 754.0, 'ratio': 1.468571401200456, 'them': 2632.0},
        #   'engagement': 1.092512573454271,
        #   'exclamations': {'me': 3900.0, 'ratio': 1.1970931674453558, 'them': 7345.0},
        #   'history': [],
        #   'id': 'all',
        #   'interest': 1.2255897401402462,
        #   'length': {'me': 24.79492389997097,
        #              'ratio': 1.0063605993035918,
        #              'them': 25.112364161659936},
        #   'links': {'me': 158.0, 'ratio': 1.537780918665469, 'them': 664.0},
        #   'questions': {'me': 9840.0, 'ratio': 0.88536496615504, 'them': 9873.0},
        #   'repeat': {'me': 0.5374486791357359,
        #              'ratio': 1.0818155637855993,
        #              'them': 0.6332282740733445},
        #   'response': {'me': 0, 'ratio': 1, 'them': 0},
        #   'response_score': {'me': 0, 'ratio': 1, 'them': 0},
        #   'responsiveness': 1.0,
        #   'texts': {'me': 48226.0, 'ratio': 1.128361237950279, 'them': 60918.0},
        #   'total': 1.0951740654466349}
        filtered_data = [a_json_dict for a_json_dict in data if a_json_dict['id'] == "all"][0]
        user_stats_record_for_me, is_created = models.UserStatsRecord.objects.get_or_create(user=user, is_for_me=True)
        user_stats_record_for_them, is_created = models.UserStatsRecord.objects.get_or_create(user=user, is_for_me=False)
        self._save_user_stats_to_db_helper(userstatsrecord=user_stats_record_for_me, p_dict=filtered_data, is_for_me=True)
        self._save_user_stats_to_db_helper(userstatsrecord=user_stats_record_for_them, p_dict=filtered_data,
                                           is_for_me=False)

    def _format_json(self, items):
        data = [{
            'address': item.address,
            'name': item.name,
            'type': item.type,
            'date': item.datetime.strftime(DATETIME_FORMAT),
            'body': item.body,
        } for item in items]
        return Response(data, content_type='text/json')

    def _format_xsl(self, items):
        field_to_column = OrderedDict([
            ('address', 'A'),
            ('name', 'B'),
            ('type', 'C'),
            ('datetime', 'D'),
            ('body', 'E'),
        ])

        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        worksheet.set_column('A:A', width=20)
        worksheet.set_column('B:B', width=20)
        worksheet.set_column('C:C', width=10)
        worksheet.set_column('D:D', width=20)
        worksheet.set_column('E:E', width=100)

        date_format = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm:ss'})

        for i, item in enumerate(items, start=1):
            for key, col in field_to_column.items():
                if key == 'datetime':
                    worksheet.write_datetime(
                        col + str(i),
                        item.datetime.replace(tzinfo=None),
                        date_format,
                    )
                else:
                    worksheet.write(col + str(i), getattr(item, key))

        workbook.close()

        ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        return HttpResponse(output.getvalue(), content_type=ct, )

    # This is called when the user wants to analyze a contact and get a score
    # We change the score with the upvote/downvote adjustment here in this 
    # function where we have access to the contact table information. The adjustment
    # is done at a contact level.
    def _format_stats(self, user, addresses, ignore_cache=False):
        user = get_user(user)
        items = models.SMSLog.objects.filter(user=user)

        a_user_stats, is_created = models.UserStatsCache.objects.get_or_create(user=user)

        if not ignore_cache:  # get score_for_all from db
            cached_values_for_all = CachedValues(total=a_user_stats.score_for_all,
                                                 engagement=a_user_stats.engagement_for_all,
                                                 interest=a_user_stats.interest_for_all)
            if addresses:
                items = items.filter(address__in=addresses)

            ordered_items = items.values('type', 'body', 'datetime', 'address', 'name', 'picture_count').order_by('address', 'datetime')

            data, dummy = get_stats(ordered_items,addresses, cached_values_for_all=cached_values_for_all)
            
            #if the user has a contact saved in a slot
            if addresses:
                for tmp_address in addresses:
                    tmp_data = [x for x in data if x['id'] == tmp_address][0]
                    if models.Contact.objects.filter(user=user, contact_number=tmp_address).exists():
                        tmp_contact = models.Contact.objects.get(user=user, contact_number=tmp_address)
                        if tmp_contact.vote_timestamp:
                            tmp_data['vote_timestamp'] = long(tmp_contact.vote_timestamp)
                            tmp_data['vote'] = int(tmp_contact.vote)
                            #Adjust the score at this contact level based on the contact vote
                            adjustment_for_vote = int(tmp_contact.vote) * 3 / 100
                            tmp_data['total']= float(tmp_data['total']) + adjustment_for_vote
                        else:
                            tmp_data['vote_timestamp'] = 0
                            tmp_data['vote'] = 0
                    else:
                        tmp_data['vote_timestamp'] = 0
                        tmp_data['vote'] = 0
                self._save_score_to_contact(user=user, data=data)
                self._save_contacts_stats_to_db(user=user, data=data)
            return Response(data, content_type='text/json')
        else:  # calculate score_for_all and save it to db
            ordered_items = items.values('type', 'body', 'datetime', 'address', 'name', 'picture_count').order_by('address', 'datetime')

            # Order by date to get the earliest and latest messages. These are saved in userstatscache
            ordered_dates = items.values('datetime').order_by('datetime')
            if (len(ordered_dates) > 0):
                first_day_as_date = ordered_dates[0]['datetime']
                first_day = convert_date_to_integer(first_day_as_date)
                last_index = len(ordered_dates) -1
                last_day_as_date = ordered_dates[last_index]['datetime']
                last_day = convert_date_to_integer(last_day_as_date)
            else:
                first_day_as_date = None
                first_day = None
                last_day_as_date = None
                last_day = None
                
            # Find phone call items only and order by date to get the earliest phone call. This is saved in userstatscache    
            phone_call_types = ['Incoming', 'Outgoing', 'Missed']
            ordered_calls = items.values('datetime').filter(type__in=phone_call_types).order_by('datetime')
            if (len(ordered_calls) > 0):
                first_phone_call_as_date = ordered_calls[0]['datetime']
                first_phone_call = convert_date_to_integer(first_phone_call_as_date)
                last_index = len(ordered_calls) -1
                last_phone_call_as_date = ordered_calls[last_index]['datetime']
                last_phone_call = convert_date_to_integer(last_phone_call_as_date)                
            else:
                first_phone_call = None
                first_phone_call_as_date = None
                last_phone_call = None
                last_phone_call_as_date = None
                
            # Find text message items only and order by date to get the earliest text message. This is saved in userstatscache    
            text_message_types = ['Inbox', 'Sent']
            ordered_texts = items.values('datetime').filter(type__in=text_message_types).order_by('datetime')
            if (len(ordered_texts) > 0):
                first_text_as_date = ordered_texts[0]['datetime']
                first_text = convert_date_to_integer(first_text_as_date)
                last_index = len(ordered_texts) -1
                last_text_as_date = ordered_texts[last_index]['datetime']
                last_text = convert_date_to_integer(last_text_as_date)                
            else:
                first_text = None
                first_text_as_date = None
                last_text = None
                last_text_as_date = None                
            
            data, cached_values_for_all = get_stats(ordered_items, addresses)

            a_user_stats.cache_score_and_save(cached_values_for_all, ordered_items.count(), first_day_as_date, first_day, 
                                              last_day_as_date, last_day, first_phone_call_as_date, first_phone_call,
                                              last_phone_call_as_date, last_phone_call, first_text_as_date, first_text,
                                              last_text_as_date, last_text)
            self._save_user_stats_to_db(user=user, data=data)
            
            #if the user has a contact saved in a slot
            if addresses:
                for tmp_address in addresses:
                    tmp_data = [x for x in data if x['id'] == tmp_address][0]
                    if models.Contact.objects.filter(user=user, contact_number=tmp_address).exists():
                        tmp_contact = models.Contact.objects.get(user=user, contact_number=tmp_address)
                        if tmp_contact.vote_timestamp:
                            tmp_data['vote_timestamp'] = long(tmp_contact.vote_timestamp)
                            tmp_data['vote'] = int(tmp_contact.vote)
                            #Adjust the score at this contact level based on the contact vote
                            adjustment_for_vote = int(tmp_contact.vote) * 3 / 100
                            tmp_data['total']= float(tmp_data['total']) + adjustment_for_vote
                        else:
                            tmp_data['vote_timestamp'] = 0
                            tmp_data['vote'] = 0
                    else:
                        tmp_data['vote_timestamp'] = 0
                        tmp_data['vote'] = 0
                self._save_score_to_contact(user=user, data=data)
                self._save_contacts_stats_to_db(user=user, data=data)
            return Response(data, content_type='text/json')

    def get(self, request, user_id, a_format):
        
        user_id = clear_phone_number(user_id)
        if not check_password_or_auth_token(request):
            return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            try:
                user = models.User.objects.get(user_id=user_id)
            except models.User.DoesNotExist:
                raise NotFound()

            items = models.SMSLog.objects.filter(user=user)

            addresses = request.GET.get('addresses')
            ignore_cache = request.GET.get("ignore_cache")

            if addresses:
                addresses = addresses.split(',')
                items = items.filter(address__in=addresses)

            if a_format == 'json':
                return self._format_json(items)
            elif a_format == 'xls':
                return self._format_xsl(items)
            elif a_format == 'stats':
                if ignore_cache and bool(ignore_cache):
                    return self._format_stats(user, addresses, ignore_cache=True)
                else:
                    return self._format_stats(user, addresses)
            else:
                raise BaseApiError('Invalid data format `%s`' % a_format)