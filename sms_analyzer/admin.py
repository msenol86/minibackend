
from django.contrib import admin
from django.utils.html import format_html

from .models import User, SMSLog, Contact, AppFeedback, Referral, UserStatsCache, ContactStats, UserStatsRecord


LINK_FORMAT = "<b><a href='{url}'>{url_title}</a></b>"
#This files is to set what info to display on the Django admin pages


class ContactInline(admin.TabularInline):
    model = Contact
    show_change_link = True

    def get_queryset(self, request):
        return Contact.objects.filter(relation_status__in=("friend", "family", "crush"))


class ContactStatsInline(admin.TabularInline):
    model = ContactStats
    show_change_link = True
    fields = ('is_for_me','text_messages_count','phone_calls_count',
              'average_message_length','correspondence_per_day','texts_per_day','calls_per_day',
              'response_score','emojis_percentage','question_marks_percentage','exclamations_percentage',
              'repeat_percentage', 'links_percentage', 'phone_calls_percentage', 'picture_count_percentage',
              'typed_character_count','days_of_correspondence','days_of_calling','days_of_texting',
              'emojis_count','question_marks_count','exclamations_count','initiation_count','engagement_count',
              'links_count','pictures_count','repeat_texts_count',)
    readonly_fields = ('texts_per_day',
                       'calls_per_day',
                       'correspondence_per_day',
                       'span_of_data',
                       'typed_character_count',
                       'emojis_percentage',
                       'links_percentage',
                       'question_marks_percentage',
                       'exclamations_percentage',
                       'phone_calls_percentage',
                       'picture_count_percentage',
                       'repeat_texts_count',)
    max_num = 2


class UserStatsCacheInline(admin.TabularInline):
    model = UserStatsCache
    show_change_link = True
    fields = ('score_for_all','engagement_for_all','interest_for_all',
              'message_count_before_cache', 'first_day_as_date', 'last_day_as_date','span_of_data', 
              'first_phone_call_as_date','last_phone_call_as_date', 'span_of_calls',
              'first_text_as_date','last_text_as_date', 'span_of_texts')
    readonly_fields = ('span_of_data', 'span_of_calls', 'span_of_texts')
    max_num = 1


class UserStatsRecordInline(admin.TabularInline):
    model = UserStatsRecord
    show_change_link = True
    fields = ('is_for_me','text_messages_count','phone_calls_count',
              'correspondence_per_day','texts_per_day','calls_per_day',
              'repeat_percentage','average_message_length',
              'response_score','pictures_count','emojis_count','links_count',
              'exclamations_count','question_marks_count','typed_character_count',
              'emojis_percentage','links_percentage','question_marks_percentage','exclamations_percentage',
              'phone_calls_percentage', 'picture_count_percentage',
              'repeat_texts_count',)
    readonly_fields = ('texts_per_day',
                       'calls_per_day', 
                       'correspondence_per_day',
                       'typed_character_count', 'emojis_percentage',
                       'links_percentage',
                       'question_marks_percentage',
                       'exclamations_percentage',
                       'phone_calls_percentage',
                       'picture_count_percentage',
                       'repeat_texts_count', )
    max_num = 2


class UserAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'name', 'get_signup_datetime', 'get_contacts_analyzed_count', 'get_contacts_manually_analyzed_count',
                    'get_unique_contacts_count', 'get_message_count', 'referrer', 'get_referral_count_used_by_app')
    inlines = [UserStatsRecordInline, UserStatsCacheInline, ContactInline, ]
    readonly_fields = ('get_referral_count_used_by_app', 'get_real_referral_count', 'show_referrers',
                       'get_signup_datetime', 'get_contacts_analyzed_count', 'get_contacts_manually_analyzed_count',
                       'get_unique_contacts_count', 'get_message_count',
                       'show_unspecified_contactstats', 'show_unspecified_contacts', )
    search_fields = ['user_id', 'name']
    list_per_page = 50

    def show_referrers(self, obj):
        if obj.referral_code and obj.referral_code != "":
            return format_html(LINK_FORMAT, url="/smsanalyzeradmin/sms_analyzer/user/?referrer=" + obj.referral_code,
                           url_title="Show Referrer Users")
        else:
            return None

    def show_unspecified_contactstats(self, obj):
        return format_html(LINK_FORMAT,
                           url="/smsanalyzeradmin/sms_analyzer/contactstats/?contact__relation_status__exact=unspecified&q=" + obj.user_id,
                           url_title="Show Unspecified ContactStats")

    show_unspecified_contactstats.short_description = "Unspecified Contact Stats"

    def show_unspecified_contacts(self, obj):
        return format_html(LINK_FORMAT,
                           url="/smsanalyzeradmin/sms_analyzer/contact/?relation_status__exact=unspecified&q=" + obj.user_id,
                           url_title="Show Unspecified Contacts")

    show_unspecified_contacts.short_description = "Unspecified Contacts"


class UserStatsCacheAdmin(admin.ModelAdmin):
    list_display = ('user','score_for_all', 'engagement_for_all', 'interest_for_all', 'message_count_before_cache', 'created_at', 'updated_at')
    fields = ('user','created_at','updated_at', 'score_for_all','engagement_for_all','interest_for_all',
              'message_count_before_cache', 'first_day_as_date', 'last_day_as_date','span_of_data', 
              'first_phone_call_as_date','last_phone_call_as_date', 'span_of_calls',
              'first_text_as_date','last_text_as_date', 'span_of_texts')
    readonly_fields = ('span_of_data', 'span_of_calls', 'span_of_texts','created_at', 'updated_at')


class UserStatsRecordAdmin(admin.ModelAdmin):
    list_display = ('user', 'is_for_me', 'text_messages_count', 'created_at', 'updated_at')
    readonly_fields = ('texts_per_day', 'calls_per_day','correspondence_per_day', 'typed_character_count', 'emojis_percentage',
                       'links_percentage',
                       'question_marks_percentage',
                       'exclamations_percentage',
                       'phone_calls_percentage', 'picture_count_percentage',
                       'repeat_texts_count', 'created_at', 'updated_at')


class SMSLogAdmin(admin.ModelAdmin):
    list_display = ('user', 'address', 'name', 'datetime', 'type', 'picture_count', 'keyword_count', 'g_polarity', 'w_score', 'upload_type', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    search_fields = ['address', 'user__user_id']
    exclude = ('body',)


class ContactStatsAdmin(admin.ModelAdmin):
    list_display = ('contact', 'is_for_me', 'created_at', 'updated_at')
    fields = ('contact','is_for_me','created_at','updated_at',
              'first_day', 'first_day_as_date', 'last_day', 'last_day_as_date','span_of_data', 
              'text_messages_count','phone_calls_count',
              'days_of_texting','days_of_calling','days_of_correspondence',
              'texts_per_day', 'calls_per_day', 'correspondence_per_day',
              'pictures_count','emojis_count','links_count',
              'exclamations_count','initiation_count','engagement_count',
              'question_marks_count','response_score','repeat_percentage',
              'average_message_length','typed_character_count', 'emojis_percentage',
              'links_percentage', 'question_marks_percentage','exclamations_percentage',
              'phone_calls_percentage', 'picture_count_percentage','repeat_texts_count',
              )
    readonly_fields = ('contact', 
                       'first_day', 'first_day_as_date', 'last_day', 'last_day_as_date',
                       'span_of_data', 'days_of_texting', 'days_of_calling', 'days_of_correspondence', 
                       'texts_per_day', 'calls_per_day', 'correspondence_per_day',
                       'typed_character_count', 'emojis_percentage',
                       'links_percentage',
                       'question_marks_percentage',
                       'exclamations_percentage',
                       'phone_calls_percentage', 'picture_count_percentage',
                       'repeat_texts_count',
                       'created_at', 'updated_at')
    search_fields = ('contact__user__user_id', )
    list_filter = ('contact__relation_status', )

    def go_to_user(self, obj):
        return format_html(LINK_FORMAT,
                           url="/smsanalyzeradmin/sms_analyzer/user/" + str(obj.contact.user.pk),
                           url_title="Go to User")

    go_to_user.short_description = "Go to User"

    def go_to_contact(self, obj):
        return format_html(LINK_FORMAT,
                           url="/smsanalyzeradmin/sms_analyzer/contact/" + str(obj.contact.pk),
                           url_title="Go to Contact")

    go_to_contact.short_description = "Go to User"



class ContactAdmin(admin.ModelAdmin):
    list_display = ('user', 'contact_number')
    inlines = [ContactStatsInline]
    search_fields = ['user__user_id']
    readonly_fields = ('span_of_data', 'go_to_user', 'get_vote_datetime')
    list_filter = ('relation_status',)

    def go_to_user(self, obj):
        return format_html(LINK_FORMAT,
                           url="/smsanalyzeradmin/sms_analyzer/user/" + str(obj.user.pk),
                           url_title="Go to User")

    go_to_user.short_description = "Go to User"


class AppFeedbackAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'content', 'created_at')


class ReferralAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'referree', 'created_at')

admin.site.register(AppFeedback, AppFeedbackAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(SMSLog, SMSLogAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Referral, ReferralAdmin)
admin.site.register(UserStatsCache, UserStatsCacheAdmin)
admin.site.register(ContactStats, ContactStatsAdmin)
admin.site.register(UserStatsRecord, UserStatsRecordAdmin)
