# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division, print_function, with_statement
import re
import datetime
import pickle
from datetime import date
from sms_analyzer import models
import sys
import logging
from rest_framework.authtoken.models import Token
from django.http import HttpRequest
from rest_framework.request import Request

logger = logging.getLogger(__name__)

EPOCH_START_IND = date(1969, 12, 31).toordinal()

if sys.maxunicode == 65535:
    # support narrow python build
    BASE_REGEX = r'[\u2600-\u27BF]|(?:\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDE4F])|(?:\uD83D[\uDE80-\uDEFF])'

else:
    BASE_REGEX = r'[\U00002600-\U000027BF]|[\U0001f300-\U0001f64F]|[\U0001f680-\U0001f6FF]'

TEXT_EMOTICONS = [
    ':-)', ':)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}', ':^)',
    ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', '=-D', '=D', '=-3', '=3', 'B^D',
]

re_emoticons = re.compile(
    BASE_REGEX + r'|'
    + r'|'.join('(%s)' % re.escape(emot) for emot in TEXT_EMOTICONS),
    re.UNICODE,
)


re_links = re.compile(
    r'((https?|ftp)://|(www|ftp)\.)?[a-z0-9-]+(\.[a-z0-9-]+)+([^ ]*)',
    re.IGNORECASE,
)

DATETIME_FORMAT = '%m/%d/%Y %H:%M:%S'
PASSWORD = "p9xBDzW5VZSy4gwqx2snDGeGzGKHHkNF"
TEMP_PASSWORD = "0b58eb7efc696b1bc3f2cd40c0ad5fab"

CSV_FIELDS_WITH_OFFSET_DICT = [
        'date', 'address', 'name', 'type', 'body', 'duration',
        'numbertype', 'is_read', 'new', 'numberlabel',  'Images', 'timezone_offset',
        'source', 'upload_type', 'g_polarity', 'g_magnitude', 'w_mixed', 'w_score', 'w_type']


def check_password_or_auth_token(request, json_load=None):
    token_on_db = Token.objects.get(user__username="mobile")
    if request.auth and str(request.auth) == token_on_db.key:
        return True
    elif json_load:
        if 'password' in json_load and json_load['password'].strip() == PASSWORD:
            return True
        else:
            return False
    else:
        if hasattr(request, 'POST') and 'password' in request.POST and request.POST['password'] == PASSWORD:
            return True
        elif hasattr(request, 'GET') and 'password' in request.GET and request.GET['password'] == PASSWORD:
            return True
        else:
            return False


# _test_request_with_token = Request(HttpRequest())
# _test_request_with_token.auth = Token.objects.get(user__username="mobile").key
# assert check_password_or_auth_token(request=_test_request_with_token)
# _test_request_with_token.auth = "232"
# assert not check_password_or_auth_token(request=_test_request_with_token)
# _test_request_with_token.auth = None
# assert not check_password_or_auth_token(request=_test_request_with_token)
#
# _test_request_with_url_pwd = Request(HttpRequest())
# _test_request_with_url_pwd.POST['password'] = PASSWORD
# assert check_password_or_auth_token(_test_request_with_url_pwd)
# _test_request_with_url_pwd.POST['password'] = "dummy"
# assert not check_password_or_auth_token(_test_request_with_url_pwd)
# del _test_request_with_url_pwd.POST['password']
# assert not check_password_or_auth_token(_test_request_with_url_pwd)
#
# _test_request_with_url_json = Request(HttpRequest)
# assert check_password_or_auth_token(_test_request_with_url_json, {"password": PASSWORD})
# assert not check_password_or_auth_token(_test_request_with_url_json)
# assert not check_password_or_auth_token(_test_request_with_url_json, {"password": "dfds"})


def clear_phone_number(phone_number):
    """
        Delete all characters except digits from given string. Then take rightmost 10 digits and return it
    :return:
    """
    only_digits = re.sub("\D", "", phone_number)
    return only_digits[-10:]

_turkish_phone_number = u"+905377684641"
assert clear_phone_number(_turkish_phone_number) == u"5377684641"


def clean_datetime_field(item_dict):
    """ Sets tzinfo=None for datetime fields"""
    for a_key, a_value in item_dict.items():
        if isinstance(a_value, datetime.datetime):
            item_dict[a_key] = item_dict[a_key].replace(tzinfo=None)


def get_test_items():
    with open("raw_items_as_pickle_data.txt", "rb") as tmp_f:
        all_items = pickle.load(tmp_f)
    return all_items


def convert_date_integer_to_date(date_integer):
    return date.fromordinal(date_integer + EPOCH_START_IND)

assert convert_date_integer_to_date(17026) == datetime.date(2016, 8, 12)


def convert_date_to_integer(a_date):
    return a_date.toordinal() - EPOCH_START_IND

assert convert_date_to_integer(datetime.date(2016, 8, 12)) == 17026


def clean_sms_log_type(type_str):
    if type_str == "Inbox-picture":
        return "Inbox"
    elif type_str == "Sent-picture":
        return "Sent"
    else:
        return type_str

assert clean_sms_log_type("Inbox-picture") == "Inbox"
assert clean_sms_log_type("Sent-picture") == "Sent"
assert clean_sms_log_type("dummy") == "dummy"


def generate_referral_code(user_id):
    return user_id[-5:]

assert generate_referral_code("5377684641") == '84641'
assert generate_referral_code("1234") == "1234"
assert generate_referral_code("1") == "1"


def get_user(user_id_or_or_user_object):
    if isinstance(user_id_or_or_user_object, (basestring, int)):
        return models.User.objects.get(user_id=user_id_or_or_user_object)
    else:
        return user_id_or_or_user_object

