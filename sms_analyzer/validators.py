# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from functools import wraps

from formencode.declarative import singletonmethod
from formencode.schema import Schema as fSchema
from formencode.validators import *


def _patch_validator_message(f):
    @wraps(f)
    def _patched(self, msgName, state, **kw):
        return (msgName, f(self, msgName, state, **kw))

    return singletonmethod(_patched)


def _patch_invalid_init(f):
    @wraps(f)
    def _patched(self, *args, **kwargs):
        f(self, *args, **kwargs)
        try:
            self.code, self.msg = self.msg
        except ValueError:
            self.code = None

    return _patched

# Patch formencode to save Invalid key, which caused an error message
Validator.message = _patch_validator_message(Validator.__dict__['message'].func)
Invalid.__init__ = _patch_invalid_init(Invalid.__init__)


# Let's add `description` field to validator
old_validator_init = FancyValidator.__init__


def patched_validator_init(self, *args, **kwargs):
    self.description = kwargs.pop('description', None)
    return old_validator_init(self, *args, **kwargs)

FancyValidator.__init__ = patched_validator_init


class Schema(fSchema):
    allow_extra_fields = True
    filter_extra_fields = True


class SimpleFileValidator(FancyValidator):
    accept_iterator = True

    def to_python(self, value, state=None):
        if isinstance(value, list):
            value = value[0]

        return value
