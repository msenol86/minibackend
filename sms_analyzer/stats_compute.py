from __future__ import division, unicode_literals



def avg(*args):
    if not args:
        return 0
    return sum(args) / len(args)


def list_avg(items):
    if not items:
        return 0
    return float(sum(items) / len(items))


def median(items):
    if not items:
        return 0

    return sorted(items)[int(len(items) / 2)]


def use_floor(average, floor, rel_factor):
    if not average or not rel_factor:
        return 0
    elif average < floor and floor:
        return rel_factor * average/floor
    else:
        return rel_factor


def get_ratio(me_val, them_val):
    s = me_val + them_val
    if not s:
        return 1

    return 2 * them_val / s


# computes the "value" of stats at any given time, used to output the current and previous values
def compute_response(stats, p_them, p_me, full_data=True):

    me = p_me
    them = p_them

    # set weights and constants at the beginning for easy tweaking

    engagement_rel_factors = {
        'text_and_phone': 1.5,  # M22
        'length_text': 0.5,  # M25
        'repeat_texts': 1.0  # M28
    }

    engagement_min_freqs = {
        'text_and_phone': 0.0,  # N22
        'length_text': 0.0,  # N25
        'repeat_texts': 0.1  # N28
    }

    response_time_rel_factor = 1.0  # M52
    response_time_min_freq = 0.05  # N52

    response_score_rel_factor = 1.0
    response_score_min_freq = 0.05

    indicators_rel_factors = {
        'q_marks_text': 0.2,  # M41
        'e_marks_text': 0.3,  # M42
        'emoji_text': 0.35,  # M43
        # 'pictures_text':    0.55,   # M44
        'links_text': 0.6  # M45
    }

    indicators_min_freqs = {
        'q_marks_text': 0.05,  # N41
        'e_marks_text': 0.05,  # N42
        'emoji_text': 0.05,  # N43
        # 'pictures_text':    0.05,   # N44
        'links_text': 0.05  # N45
    }

    # engagement
    texts_me = stats['count'][me]  # D19
    texts_them = stats['count'][them]  # E19
    calls_me = stats['phone_calls'][me]
    calls_them = stats['phone_calls'][them]
    
    #The phone_call_multiplier has a floor of 1 and a cap of 5
    # In between use the ratio of text messages sent by both parties to phone calls made
    if (calls_me + calls_them) == 0:
        phone_call_multiplier = 5
    elif (texts_me + texts_them) == 0:
        phone_call_multiplier = 1
    else:
        phone_call_multiplier = (texts_them + texts_me)/(calls_them + calls_me)
                
    if (phone_call_multiplier < 1):
        phone_call_multiplier = 1
    elif (phone_call_multiplier > 5):
        phone_call_multiplier = 5

    
    total_sum = sum(stats['count']) + phone_call_multiplier * sum(stats['phone_calls'])  # F22

    text_and_phone_avg = (texts_them + texts_me + phone_call_multiplier * (
    stats['phone_calls'][them] + stats['phone_calls'][me])) / 2  # G22
    count_ratio = 2 * (
    stats['count'][them] + phone_call_multiplier * stats['phone_calls'][them]) / total_sum if total_sum else 1  # K22

    average_length = [
        stats['total_length'][me] / texts_me if texts_me else 1,  # D25
        stats['total_length'][them] / texts_them if texts_them else 1,  # E25
    ]

    phone_calls_ratio = get_ratio(*stats['phone_calls'])  # K20
    length_ratio = get_ratio(*average_length)  # K25

    picture_count_ratio = get_ratio(*stats['picture_count'])

    repeat = [
        stats['consecutive'][me] / texts_me if texts_me else 1,  # D28
        stats['consecutive'][them] / texts_them if texts_them else 1,  # E28
    ]
    repeat_ratio = get_ratio(*repeat)  # K28
    engagement_averages = {
        'text_and_phone': text_and_phone_avg,  # G22
        'length_text': float(sum(average_length) / len(average_length)),  # G25
        'repeat_texts': float(sum(repeat) / len(repeat))  # G28
    }
    engagement_ratios = {
        'text_and_phone': count_ratio,  # K22
        'length_text': length_ratio,  # K25
        'repeat_texts': repeat_ratio  # K28
    }
    engagement_adj_factors = {k: use_floor(engagement_averages[k], engagement_min_freqs[k], engagement_rel_factors[k])
                              for k in engagement_rel_factors.keys()}  # O22, 25, 28
    engagement_adj_results = {k: engagement_adj_factors[k] * engagement_ratios[k] for k in
                              engagement_ratios.keys()}  # P22, 25, 28
    total_engagement_adj_factor = sum(engagement_adj_factors.itervalues())  # O30, in final_res
    total_engagement_adj_result = sum(
        engagement_adj_results.itervalues()) / total_engagement_adj_factor if total_engagement_adj_factor else 1  # P30, in final_res

    # response times
    median_resp_time_me = median(stats['response_times'][me])  # D52
    median_resp_time_them = median(stats['response_times'][them])  # E52
    # NEED EDIT - If either median_resp_time_me or median_resp_time_them = 0, then the other one is also zero (avoids basically having first person to ask a question skew the ratio)
    median_sum = median_resp_time_me + median_resp_time_them  # F52
    resp_time_ratio = 2 * median_resp_time_me / median_sum if median_sum else 1  # K52
    # resp_time_adj_factor_old = use_floor((median_resp_time_me + median_resp_time_them)/2, response_time_min_freq, response_time_rel_factor)  # O52, in final_res

    # SWL edit to add floor and ceiling
    if resp_time_ratio < 0.75:
        resp_time_ratio = 0.75
    elif resp_time_ratio > 1.25:
        resp_time_ratio = 1.25

    resp_time_adj_factor = use_floor(avg(stats['question_marks'][me] / texts_me if texts_me else 0,
                                         stats['question_marks'][them] / texts_them if texts_them else 0),
                                     response_time_min_freq, response_time_rel_factor)
    # NEED EDIT - resp_time_adj_factor calculation needs to be changed to be equal to use_floor(parameters = same as question market ratio)

    resp_time_adj_result = resp_time_ratio * resp_time_adj_factor  # P52, in final_res

    # response scores
    avg_resp_score_me = list_avg(stats['response_scores'][me])
    avg_resp_score_them = list_avg(stats['response_scores'][them])
    # NEED EDIT - If either median_resp_time_me or median_resp_time_them = 0, then the other one is also zero (avoids basically having first person to ask a question skew the ratio)
    avg_score_sum = avg_resp_score_me + avg_resp_score_them
    resp_score_ratio = 2 * avg_resp_score_them / avg_score_sum if avg_score_sum else 1  # K52

    resp_score_adj_factor = use_floor(avg(stats['question_marks'][me] / texts_me if texts_me else 0,
                                          stats['question_marks'][them] / texts_them if texts_them else 0),
                                      response_score_min_freq, response_score_rel_factor)
    resp_score_adj_result = resp_score_ratio * resp_score_adj_factor

    # indicators of interest
    question_ratio = get_ratio(stats['question_marks'][me] / texts_me if texts_me else 1,
                               stats['question_marks'][them] / texts_them if texts_them else 1)  # K41
    excl_marks_ratio = get_ratio(stats['excl_marks'][me] / texts_me if texts_me else 1,
                                 stats['excl_marks'][them] / texts_them if texts_them else 1)  # K42
    emojis_ratio = get_ratio(stats['emojis'][me] / texts_me if texts_me else 1,
                             stats['emojis'][them] / texts_them if texts_them else 1)  # K43
    # pictures_ratio = get_ratio(stats['pictures'][me]/texts_me if texts_me else 1, stats['pictures'][them]/texts_them if texts_them else 1)  # K43
    links_ratio = get_ratio(stats['links'][me] / texts_me if texts_me else 1,
                            stats['links'][them] / texts_them if texts_them else 1)  # K45

    indicators_averages = {
        'q_marks_text': avg(stats['question_marks'][me] / texts_me if texts_me else 0,
                            stats['question_marks'][them] / texts_them if texts_them else 0),  # G41
        'e_marks_text': avg(stats['excl_marks'][me] / texts_me if texts_me else 0,
                            stats['excl_marks'][them] / texts_them if texts_them else 0),  # G42
        'emoji_text': avg(stats['emojis'][me] / texts_me if texts_me else 0,
                          stats['emojis'][them] / texts_them if texts_them else 0),  # G43
        # 'pictures_text':    avg(stats['pictures'][me]/texts_me if texts_me else 0, stats['pictures'][them]/texts_them if texts_them else 0),    # G44
        'links_text': avg(stats['links'][me] / texts_me if texts_me else 0,
                          stats['links'][them] / texts_them if texts_them else 0),  # G45
    }

    indicators_ratios = {
        'q_marks_text': question_ratio,
        'e_marks_text': excl_marks_ratio,
        'emoji_text': emojis_ratio,
        # 'pictures_text':    pictures_ratio,
        'links_text': links_ratio
    }

    indicators_adj_factors = {k: use_floor(indicators_averages[k], indicators_min_freqs[k], indicators_rel_factors[k])
                              for k in indicators_averages.keys()}  # O41-45
    indicators_adj_results = {k: indicators_ratios[k] * indicators_adj_factors[k] for k in
                              indicators_ratios.keys()}  # P41-45
    # total_indicators_adj_factor = sum(indicators_adj_factors.itervalues())  # O49, in final_res

    # new condition to make sure interest indicators weighted appropriately
    #if sum(indicators_averages.itervalues()) < .25:  # so if frequency of interest is below floor
    total_indicators_adj_factor = sum(indicators_adj_factors.itervalues())
    #else:
    #    total_indicators_adj_factor = 2

    if sum(indicators_adj_factors.itervalues()): # if sum is not zero
        total_indicators_adj_result = sum(indicators_adj_results.itervalues()) / sum(indicators_adj_factors.itervalues())
    else:
        total_indicators_adj_result = 1 # P49, in final_res

    # final output value
    final_res = (
                    total_engagement_adj_factor * total_engagement_adj_result + total_indicators_adj_factor * total_indicators_adj_result + resp_score_adj_factor * resp_score_adj_result) / (
                    total_engagement_adj_factor + total_indicators_adj_factor + resp_score_adj_factor) if (
        total_engagement_adj_factor + total_indicators_adj_factor + resp_score_adj_factor) else 0

    # initiation_count and engagement_count
    initiation_count_me = stats['initiation_count'][me]
    initiation_count_them = stats['initiation_count'][them]
    initiation_count_ratio = get_ratio(initiation_count_me, initiation_count_them)

    engagement_count_me = stats['engagement_count'][me]
    engagement_count_them = stats['engagement_count'][them]
    engagement_count_ratio = get_ratio(engagement_count_me, engagement_count_them)

    days_of_texting_ratio = get_ratio(stats['days_of_texting'][me], stats['days_of_texting'][them])
    days_of_calling_ratio = get_ratio(stats['days_of_calling'][me], stats['days_of_calling'][them])
    days_of_correspondence_ratio = get_ratio(stats['days_of_correspondence'][me], stats['days_of_correspondence'][them])    
    
    if not full_data:
        return final_res

    data = {
        'texts': {
            'ratio': count_ratio,
            'me': stats['count'][me],
            'them': stats['count'][them],
        },
        'calls': {
            'ratio': phone_calls_ratio,
            'me': stats['phone_calls'][me],
            'them': stats['phone_calls'][them],
        },
        'picture_count': {
            'ratio': picture_count_ratio,
            'me': stats['picture_count'][me],
            'them': stats['picture_count'][them],
        },
        'length': {
            'ratio': length_ratio,
            'me': average_length[me],
            'them': average_length[them],
        },
        'questions': {
            'ratio': question_ratio,
            'me': stats['question_marks'][me],
            'them': stats['question_marks'][them],
        },
        'exclamations': {
            'ratio': excl_marks_ratio,
            'me': stats['excl_marks'][me],
            'them': stats['excl_marks'][them],
        },
        'emojis': {
            'ratio': emojis_ratio,
            'me': stats['emojis'][me],
            'them': stats['emojis'][them],
        },
        'links': {
            'ratio': links_ratio,
            'me': stats['links'][me],
            'them': stats['links'][them],
        },
        'repeat': {
            'ratio': repeat_ratio,
            'me': repeat[me],
            'them': repeat[them],
        },
        'response': {
            'ratio': resp_time_ratio,
            'me': median_resp_time_me,
            'them': median_resp_time_them,
        },
        'response_score': {
            'ratio': resp_score_ratio,
            'me': avg_resp_score_me,
            'them': avg_resp_score_them,
        },
        'initiation_count': {
            'ratio': initiation_count_ratio,
            'me': initiation_count_me,
            'them': initiation_count_them,
        },
        'engagement_count': {
            'ratio': engagement_count_ratio,
            'me': engagement_count_me,
            'them': engagement_count_them,
        },
        'days_of_texting': {
            'ratio': days_of_texting_ratio,
            'me': stats['days_of_texting'][me],
            'them': stats['days_of_texting'][them],                          
        },
        'days_of_calling': {
            'ratio': days_of_calling_ratio,
            'me': stats['days_of_calling'][me],
            'them': stats['days_of_calling'][them],                          
        },
        'days_of_correspondence': {
            'ratio': days_of_correspondence_ratio,
            'me': stats['days_of_correspondence'][me],
            'them': stats['days_of_correspondence'][them],                          
        },        
        'engagement': total_engagement_adj_result,
        'interest': total_indicators_adj_result,
        'responsiveness': resp_time_adj_result,
        'total': final_res,
    }

    return data
