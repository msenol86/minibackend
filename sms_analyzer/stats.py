# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division

from utils import re_emoticons, re_links
import logging
import pickle
import datetime
import copy
from copy import deepcopy
from utils import get_test_items, clean_datetime_field, convert_date_to_integer, convert_date_integer_to_date
from collections import namedtuple, deque
import itertools

import stats_compute

logger = logging.getLogger(__name__)

PHONE_CALL = 0
MESSAGE = 1
ME = 0
THEM = 1

# CONSTANTS FOR VISUAL HISTORY CALCULATION
DAYS_LIMIT_FOR_VISUAL_HISTORY = 30
TRAILING_NUMBER = 600
##

ItemRecord = namedtuple('ItemRecord', 'type address datetime sender emojis excl_marks question_marks links '
                                      'picture_count body_length is_consecutive response_score response_time '
                                      'initiation_count')
CachedValues = namedtuple('CachedValues', 'total engagement interest')

# initializes the stats to be 0. For each new contact, values need to be initialized
def get_default_stats():
    return {
        'count': [0, 0],
        'daily_count_text': [0, 0],
        'daily_count_phone': [0, 0],
        'total_length': [0, 0],
        'question_marks': [0, 0],
        'excl_marks': [0, 0],
        'emojis': [0, 0],
        'consecutive': [0, 0],
        'response_times': [[], []],
        'response_scores': [[], []],
        'prev_msg_type': None,
        'prev_is_question': False,
        'prev_datetime': None,
        'prev_address': None,
        'phone_calls': [0, 0],
        'links': [0, 0],
        'picture_count': [0, 0],
        'initiation_count': [0, 0],
        'engagement_count': [0, 0],
        'days_of_texting': [0, 0],
        'days_of_calling': [0, 0],
        'days_of_correspondence': [0, 0],
    }


def get_special_symbol_count(message_body, symbol, maximum_cap):
    """
    :param message_body: str
    :param symbol: str something like "!" or "?"
    :param maximum_cap int
    :return: number of symbol in message_body (capped at maximum_cap)
    """
    if message_body:
        excl_mark_count = message_body.count(symbol)
        return float(min(excl_mark_count, maximum_cap))
    else:
        return 0.0


def get_owner_of_sms_log_item(sms_log_item):
    tmp_owner = THEM if sms_log_item['type'] in ('Inbox', 'Incoming', 'Missed') else ME
    return tmp_owner


def process_one_item(sms_log_item):
    """
    Returns a dict like this:
    {"type": item["type"],
     "owner:": ME,
     "address": item["address"],
     "datetime": item["datetime"],
     "total_length": 1,
     "links": 2,
     "emojis": 3,
     "question_marks": 0,
     "picture_count": 0,
     "is_question": False}
    :param sms_log_item: SMS_LOG item
    :return: processed_item
    """
    item_result_dict = dict()
    body = sms_log_item['body']
    links_count = len(list(re_links.finditer(body))) if body else 0
    emojis_count = len(re_emoticons.findall(body)) if body else 0

    is_for_me = THEM if sms_log_item['type'] in ('Inbox', 'Incoming', 'Missed') else ME
    item_result_dict['owner'] = is_for_me
    item_result_dict['type'] = sms_log_item['type']
    item_result_dict['address'] = sms_log_item['address']
    item_result_dict['datetime'] = sms_log_item['datetime']
    item_result_dict['picture_count'] = sms_log_item['picture_count']

    # if text contains link, then assume text is 1 character in length (not perfect, but a simple solution for now)
    if links_count == 0:
        item_result_dict['total_length'] = len(body)
    else:
        item_result_dict['total_length'] = 1

    item_result_dict['links'] = float(min(links_count, 2))
    item_result_dict['emojis'] = float(min(emojis_count, 3))
    item_result_dict['excl_marks'] = get_special_symbol_count(body, '!', 3)
    item_result_dict['question_marks'] = get_special_symbol_count(body, '?', 3)
    item_result_dict['is_question'] = bool(item_result_dict['question_marks'])

    return item_result_dict


# def _get_items_from_db(user_id):
#     items = SMSLog.objects.filter(
#         user__user_id=user_id,
#     )
#
#     addresses = ["9144396658"]
#     if addresses:
#         items = items.filter(address__in=addresses)
#
#     items = items.values('type', 'body', 'datetime', 'address', 'name').order_by('address', 'datetime')
#     return items


def _save_items_to_file(items, file_pointer):
    for an_item in items:
        clean_datetime_field(an_item)
    pickle.dump(items, file_pointer)
    file_pointer.close()


def _load_items_from_file(file_pointer):
    items = pickle.load(file_pointer)
    return items


def _load_items_from_file_path(file_path):
    fp = open(file_path, "rb")
    items = _load_items_from_file(fp)
    fp.close()
    return items


def is_initiation(current_message, previous_message):
    if previous_message:
        if 'owner' not in previous_message:
            previous_message['owner'] = get_owner_of_sms_log_item(previous_message)
        if previous_message['owner'] == current_message['owner']:
            time_delta = current_message['datetime'] - previous_message['datetime']
            if time_delta.seconds < 900:  # 15 minutes = 900 seconds
                return 0
            else:
                return 1
        else:
            return 0
    else:
        return 0


def is_consecutive(current_message, previous_message):
    if previous_message:
        if previous_message['address'] == current_message['address']:
            return 1.0 if previous_message['type'] == current_message['type'] else 0.0
        else:
            return 0.0
    else:
        return 0.0


def calculate_engagement_count(stats, owner):
    return stats['count'][owner] - stats['consecutive'][owner] + stats['initiation_count'][owner]


def calculate_response_time(processed_current_message, processed_previous_message):
    if processed_previous_message:
        return (processed_current_message['datetime'] - processed_previous_message['datetime']).seconds / 60.
    else:
        return 0.0


def calculate_response_score(resp_mins):
    if resp_mins < 1:
        resp_score = 10.
    elif resp_mins < 5:
        resp_score = 10 - ((resp_mins - 1) / 4)
    elif resp_mins < 15:
        resp_score = 9 - ((resp_mins - 5) / 10)
    elif resp_mins < 30:
        resp_score = 8 - ((resp_mins - 15) / 15)
    elif resp_mins < 60:
        resp_score = 7 - ((resp_mins - 30) / 30)
    elif resp_mins < 120:
        resp_score = 6 - ((resp_mins - 60) / 60)
    elif resp_mins < 360:
        resp_score = 5 - ((resp_mins - 120) / 240)
    elif resp_mins < 720:
        resp_score = 4 - ((resp_mins - 360) / 360)
    elif resp_mins < 1440:
        resp_score = 3 - ((resp_mins - 720) / 720)
    elif resp_mins < 2880:
        resp_score = 2 - ((resp_mins - 1440) / 1440)
    elif resp_mins < 5760:
        resp_score = 1 - ((resp_mins - 2880) / 2800)
    else:
        resp_score = 0.01

    return resp_score


def add_response_info_to_item(processed_current_item, previous_item):
    processed_current_item['initiation_count'] = is_initiation(processed_current_item, previous_item)
    processed_current_item['consecutive'] = is_consecutive(processed_current_item, previous_item)
    processed_current_item['response_time'] = calculate_response_time(processed_current_item,
                                                                      previous_item)
    processed_current_item['response_score'] = calculate_response_score(processed_current_item['response_time'])
    return processed_current_item


def fill_stat_fields(current_stat, processed_item, message_owner, field_name):
    """
    !!! mutates current_stat
    """
    current_stat[field_name][message_owner] += float(processed_item[field_name])


def initialize_required_dicts(items):
    # intialize 'all'
    stats = {'all': get_default_stats()}

    values_history = {'all': []}
    dict_of_list__of_item_records = dict()

    for item in items:
        if item['address'] not in stats:
            stats[item['address']] = get_default_stats()  # initialize data on that address
            values_history[item['address']] = []
            dict_of_list__of_item_records[item['address']] = deque()

    return stats, values_history, dict_of_list__of_item_records


def is_a_message(an_item):
    if an_item['type'] in ['Inbox', 'Sent']:
        return True
    else:
        return False

#Calculate trailing statistics
def aggregate_list_of_item_records(dict_of_list_of_item_records, trailing_number, address, item_date):
    """
    :return: something like this {
        'count': [0, 0],
        'daily_count_text': [0, 0],
        'daily_count_phone': [0, 0],
        'total_length': [0, 0],
        'question_marks': [0, 0],
        'excl_marks': [0, 0],
        'emojis': [0, 0],
        'consecutive': [0, 0],
        'response_times': [[], []],
        'response_scores': [[], []],
        'prev_msg_type': None,
        'prev_is_question': False,
        'prev_datetime': None,
        'prev_address': None,
        'phone_calls': [0, 0],
        'links': [0, 0],
        'picture_count': [0, 0],
    }
    """
    # ItemRecord fields: 'type address datetime sender emojis excl_marks question_marks links picture_count body_length is_consecutive response_score response_time'


    list_of_item_records = dict_of_list_of_item_records[address]

    list_of_item_records = [tmp_item_record for tmp_item_record in list_of_item_records]

    if trailing_number + 1 < len(list_of_item_records):
        list_of_item_records = deque(itertools.islice(list_of_item_records, len(list_of_item_records) - trailing_number, len(list_of_item_records)))

    result_stats = get_default_stats()
    for tmp_sender in [ME, THEM]:
        # ****** TO CONFIRM: Is count a total of just texts (desired) or of texts + phone (suspected but not desired)
        result_stats['count'][tmp_sender] = len([an_item_record for an_item_record in list_of_item_records if
                                                 an_item_record.sender == tmp_sender])
        result_stats['daily_count_text'][tmp_sender] = len([an_item_record for an_item_record in list_of_item_records if
                                                    an_item_record.sender == tmp_sender and
                                                    an_item_record.datetime.date() == item_date and an_item_record.type == MESSAGE])
        result_stats['daily_count_phone'][tmp_sender] = len([an_item_record for an_item_record in list_of_item_records if
                                                    an_item_record.sender == tmp_sender and
                                                    an_item_record.datetime.date() == item_date and an_item_record.type == PHONE_CALL])        
        result_stats['total_length'][tmp_sender] = sum([an_item_record.body_length for an_item_record in list_of_item_records if
                                                        an_item_record.sender == tmp_sender])
        result_stats['question_marks'][tmp_sender] = sum([an_item_record.question_marks for an_item_record in list_of_item_records if
                                                          an_item_record.sender == tmp_sender])
        result_stats['emojis'][tmp_sender] = sum(
            [an_item_record.emojis for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender])
        result_stats['excl_marks'][tmp_sender] = sum(
            [an_item_record.excl_marks for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender])
        result_stats['links'][tmp_sender] = sum(
            [an_item_record.links for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender])
        result_stats['picture_count'][tmp_sender] = sum(
            [an_item_record.picture_count for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender])
        result_stats['consecutive'][tmp_sender] = sum(
            [1 for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender and an_item_record.is_consecutive])
        result_stats['phone_calls'][tmp_sender] = sum(
            [1 for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender and an_item_record.type == PHONE_CALL])

        result_stats['initiation_count'][tmp_sender] = sum(
            [1 for an_item_record in list_of_item_records if
             an_item_record.sender == tmp_sender and an_item_record.initiation_count])

        result_stats['engagement_count'][tmp_sender] = calculate_engagement_count(result_stats, tmp_sender)

        result_stats['response_times'][tmp_sender] = [an_item_record.response_time for an_item_record in list_of_item_records if
                                                      an_item_record.sender == tmp_sender]
        result_stats['response_scores'][tmp_sender] = [an_item_record.response_score for an_item_record in list_of_item_records if
                                                       an_item_record.sender == tmp_sender]

    return result_stats

# This populates a single day's date into history. Only called once per day
def update_values_history_helper(item_date, save_address, current_values_history, dict_of_list__of_item_records):
    date_integer = convert_date_to_integer(item_date)
    aggregated_stats = aggregate_list_of_item_records(dict_of_list__of_item_records, TRAILING_NUMBER, save_address, item_date)
    tmp_dict = stats_compute.compute_response(aggregated_stats, p_them=THEM, p_me=ME, full_data=True)
    total = tmp_dict['total']
    engagement = tmp_dict['engagement']
    interest = tmp_dict['interest']
    text_count_me = aggregated_stats['daily_count_text'][ME]
    text_count_them = aggregated_stats['daily_count_text'][THEM]    
    phone_count_me = aggregated_stats['daily_count_phone'][ME]
    phone_count_them = aggregated_stats['daily_count_phone'][THEM]  
    
    # If you add more things to this list or change the order then make sure to 
    # change the index numbers in increment_days_of_texting_calling_correspondence()
    # or it will break. It depends on this order. 
    current_values_history.append((date_integer, total, engagement, interest, text_count_me, text_count_them, phone_count_me, phone_count_them))

    
def increment_days_of_texting_calling_correspondence(current_values_history, current_stat):
    # Increment days_of_texting / calling if a call or text found for that day
    # Increment days_of_correspondence if either a text or a call came in
    lastHistory = current_values_history[-1]
    
    if (lastHistory[4] > 0 ): #text_count_me
        current_stat['days_of_texting'][ME] = int(current_stat['days_of_texting'][ME]) + 1
    if (lastHistory[5] > 0): #text_count_them
        current_stat['days_of_texting'][THEM] = int(current_stat['days_of_texting'][THEM]) + 1        
    if (lastHistory[6] > 0): #phone_count_me
        current_stat['days_of_calling'][ME] = int(current_stat['days_of_calling'][ME]) + 1
    if (lastHistory[7] > 0): #phone_count_them
        current_stat['days_of_calling'][THEM] = int(current_stat['days_of_calling'][THEM]) + 1  
    if (lastHistory[4] > 0 or lastHistory[6] > 0): #correspondence_count_me
        current_stat['days_of_correspondence'][ME] = int(current_stat['days_of_correspondence'][ME]) + 1
    if (lastHistory[5] > 0 or lastHistory[7] > 0): #correspondence_count_them
        current_stat['days_of_correspondence'][THEM] = int(current_stat['days_of_correspondence'][THEM]) + 1  
      

def update_values_history(current_stat, current_item, previous_item, next_item, save_address, values_history,
                          dict_of_list__of_item_records):
    if previous_item and previous_item[u'address'] == current_item[u'address']:

        if next_item and next_item[u'address'] == current_item[u'address']:
            is_last_item = False
        else:
            is_last_item = True

        total_items_for_this_address = current_stat['count'][ME] + current_stat['count'][THEM]

        previous_item_date = previous_item[u'datetime'].date()
        current_item_date = current_item[u'datetime'].date()

        current_values_history = values_history[save_address]

        if total_items_for_this_address > 1:
            if is_last_item:
                
                update_values_history_helper(item_date=previous_item_date, save_address=save_address,
                                             current_values_history=current_values_history,
                                             dict_of_list__of_item_records=dict_of_list__of_item_records)
                #if previous_item_date != current_item_date:
                increment_days_of_texting_calling_correspondence(current_values_history, current_stat)
            else:
                #Only add a value to history if it's a new day
                if next_item[u'datetime'].date() == current_item_date:
                    pass
                else:
                    if previous_item_date != current_item_date:
                        # this code added because of messages with ordinals like this [...17062,17062,17062,17063,17064,17064...]
                        # without this if check the calculated value of item 17063 saved into history as 17062
                        update_values_history_helper(item_date=current_item_date, save_address=save_address,
                                                     current_values_history=current_values_history,
                                                     dict_of_list__of_item_records=dict_of_list__of_item_records)

                    else:
                        update_values_history_helper(item_date=previous_item_date, save_address=save_address,
                                                     current_values_history=current_values_history,
                                                     dict_of_list__of_item_records=dict_of_list__of_item_records)

                    increment_days_of_texting_calling_correspondence(current_values_history, current_stat)
#_all_shiwen_items = _load_items_from_file_path("/home/msenol/Desktop/sms-analyzer-backend/sms-analyzer/all_shiwen_items.txt")
#_test_stats, _test_values_history, _test_dict = initialize_required_dicts(_all_shiwen_items)
#_test_current_item = _all_shiwen_items[25]
#_test_previous_item = _all_shiwen_items[24]
#_test_save_address = _test_current_item[u'address']
#_test_current_stat = _test_stats[_test_save_address]


def is_belongs_to_same_address(message_1, message_2):
    if message_1 and message_2:
        if message_1[u'address'] == message_2[u'address']:
            return True
        else:
            return False
    else:
        return False


def fill_a_message_into_stats(current_message, previous_item, next_item, stats, values_history, dict_of_list__of_item_records):
    """
        !!!Mutates stats, values_history, dict_of_list__of_item_records
    """
    current_message_address = current_message[u'address']
    current_message_datetime = current_message[u'datetime']
    message_owner = current_message[u'owner']
    current_message[u'count'] = 1

    current_stat = stats[current_message_address]

    fill_stat_fields(current_stat, current_message, message_owner, u'count')
    fill_stat_fields(current_stat, current_message, message_owner, u'total_length')
    fill_stat_fields(current_stat, current_message, message_owner, u'question_marks')
    fill_stat_fields(current_stat, current_message, message_owner, u'excl_marks')
    fill_stat_fields(current_stat, current_message, message_owner, u'emojis')
    fill_stat_fields(current_stat, current_message, message_owner, u'links')
    fill_stat_fields(current_stat, current_message, message_owner, u'consecutive')
    fill_stat_fields(current_stat, current_message, message_owner, u'picture_count')
    fill_stat_fields(current_stat, current_message, message_owner, u'initiation_count')

    engagement_count = calculate_engagement_count(current_stat, message_owner)
    current_stat[u'engagement_count'][message_owner] = engagement_count

    # if not the first message and previous and current message belong to same address
    if previous_item and previous_item[u'address'] == current_message_address:
        if current_message[u'consecutive'] == 0.0:  # if not consecutive
            current_stat[u'response_times'][message_owner].append(current_message[u'response_time'])
            current_stat[u'response_scores'][message_owner].append(current_message[u'response_score'])

    new_message_record = ItemRecord(type=MESSAGE,
                                    address=current_message_address,
                                    datetime=current_message_datetime,
                                    sender=message_owner,
                                    emojis=current_message[u'emojis'],
                                    excl_marks=current_message[u'excl_marks'],
                                    question_marks=current_message[u'question_marks'],
                                    links=current_message[u'links'],
                                    picture_count=current_message[u'picture_count'],
                                    body_length=current_message[u'total_length'],
                                    is_consecutive=current_message[u'consecutive'],
                                    response_time=current_message[u'response_time'],
                                    response_score=current_message[u'response_score'],
                                    initiation_count=current_message['initiation_count'])

    dict_of_list__of_item_records[current_message_address].append(new_message_record)

    update_values_history(current_stat=current_stat, current_item=current_message,
                          next_item=next_item,
                          previous_item=previous_item, save_address=current_message_address,
                          values_history=values_history, dict_of_list__of_item_records=dict_of_list__of_item_records)


def fill_a_phone_call_into_stats(current_call, previous_item, next_item, stats, values_history, dict_of_list__of_item_records):
    """
        !!!Mutates stats,  values_history, dict_of_list__of_item_records
    """
    call_address = current_call['address']
    call_datetime = current_call[u'datetime']
    call_owner = current_call[u'owner']
    current_stat = stats[call_address]

    current_call['phone_calls'] = float(1)
    fill_stat_fields(current_stat, current_call, call_owner, 'phone_calls')
    fill_stat_fields(current_stat, current_call, call_owner, 'initiation_count')
    engagement_count = calculate_engagement_count(current_stat, call_owner)
    current_stat[u'engagement_count'][call_owner] = engagement_count

    if previous_item and call_address == previous_item['address']:
        if current_call[u'consecutive'] == 0.0:
            current_stat[u'response_times'][call_owner].append(current_call[u'response_time'])
            current_stat[u'response_scores'][call_owner].append(current_call[u'response_score'])

    new_phone_record = ItemRecord(type=PHONE_CALL,
                                  address=call_address,
                                  datetime=call_datetime,
                                  sender=call_owner,
                                  emojis=0.0,
                                  excl_marks=0.0,
                                  question_marks=0.0,
                                  links=0.0,
                                  picture_count=0.0,
                                  body_length=0.0,
                                  is_consecutive=current_call[u'consecutive'],
                                  response_score=current_call[u'response_score'],
                                  response_time=current_call[u'response_time'],
                                  initiation_count=current_call['initiation_count'])

    dict_of_list__of_item_records[call_address].append(new_phone_record)

    update_values_history(current_stat=stats[call_address], current_item=current_call,
                          next_item=next_item,
                          previous_item=previous_item, save_address=call_address,
                          values_history=values_history,
                          dict_of_list__of_item_records=dict_of_list__of_item_records)


# SWL: This is called to get the score for every address in addresses (so up to 3 contacts or 'all' contacts of a user)
# SWL: At this point, stats (including in history) are already filled in.  
# SWL: Most of this function is for calculating 'all' and modifying scores (including in history) for 'all' 
# SWL: Note that the vote up / vote down
# SWL: functionality is not changed here. It is changed in stats_view where there is access to the contact
# SWL: level information. 
def calculate_response(stats, total_for_all, engagement_for_all, interest_for_all, values_history, addresses):
    response = []

    adjustment_for_total = (total_for_all - 1.0) * 25 / 100
    adjustment_for_engagement = (engagement_for_all - 1.0) * 25 / 100
    adjustment_for_interest = (interest_for_all - 1.0) * 25 / 100
    
    total_message_count_for_me = 0
    total_message_count_for_them = 0
    response_score_list_for_me = []
    response_score_list_for_them = []
    repeat_percentage_list_for_me = []
    repeat_percentage_list_for_them = []

    for address, stats_ in stats.items():
        if not addresses or (address == "all" or address in addresses):
            temp_dict = stats_compute.compute_response(stats_, p_them=THEM, p_me=ME)
            temp_dict['id'] = address

            if not addresses and address != "all":
                temp_text_count_for_me = int(temp_dict['texts']['me'])
                temp_text_count_for_them = int(temp_dict['texts']['them'])
                total_message_count_for_me += temp_text_count_for_me
                total_message_count_for_them += temp_text_count_for_them
                response_score_list_for_me.append(float(temp_dict["response_score"]["me"]) * temp_text_count_for_me)
                response_score_list_for_them.append(float(temp_dict["response_score"]["them"]) * temp_text_count_for_them)
                repeat_percentage_list_for_me.append(float(temp_dict["repeat"]["me"] * temp_text_count_for_me))
                repeat_percentage_list_for_them.append((float(temp_dict["repeat"]["them"] * temp_text_count_for_them)))

            # if address != "all":
            #     temp_dict['total'] -= adjustment

            temp_dict['history'] = [
                {
                    'ts': x[0],
                    'val': x[1] if address == "all" else x[1] - adjustment_for_total,
                    'engagement': x[2] if address == "all" else x[2] - adjustment_for_engagement,
                    'interest': x[3] if address == "all" else x[3] - adjustment_for_interest,
                    'text_count_me': x[4],
                    'text_count_them': x[5],
                    'phone_count_me': x[6],
                    'phone_count_them': x[7],                   
                } for x in values_history[address]
                ]

            if values_history[address]:  # if list's length is not 0
                last_element_in_history = sorted(values_history[address], key=lambda a: a[0])[-1]

                if address != "all":
                    temp_dict['total'] = last_element_in_history[1] - adjustment_for_total
                    temp_dict['engagement'] = last_element_in_history[2] - adjustment_for_engagement
                    temp_dict['interest'] = last_element_in_history[3] - adjustment_for_interest

            response.append(temp_dict)

    for an_item in response:
        if not addresses and an_item['id'] == "all":
            if total_message_count_for_me > 0:
                an_item["response_score"]["me"] = sum(response_score_list_for_me) / total_message_count_for_me
                an_item["repeat"]["me"] = sum(repeat_percentage_list_for_me) / total_message_count_for_me
            if total_message_count_for_them > 0:
                an_item["response_score"]["them"] = sum(response_score_list_for_them) / total_message_count_for_them
                an_item["repeat"]["them"] = sum(repeat_percentage_list_for_them) / total_message_count_for_them

    return response

#SWL: This is to aggregate stats for 'all' contacts of a User.  The dict consists of only the fields that can be summed up 
def calculate_stats_all(stats):
    result_stats_item = get_default_stats()
    for an_address, stat_dict in stats.items():
        for dict_key in ("count", "total_length", "question_marks", "excl_marks", "emojis", "consecutive",  "phone_calls", "links", "picture_count", 'initiation_count', 'engagement_count'):
            result_stats_item[dict_key][0] += stat_dict[dict_key][0]
            result_stats_item[dict_key][1] += stat_dict[dict_key][1]

    return result_stats_item

#SWL: This is called everytime the user hits analyze.  This is the highest level function in stats.py.  This will fill in stats and calculate
#SWL: scores for every address passed in
def get_stats(items, addresses, cached_values_for_all=None):
    stats, values_history, dict_of_list__of_item_records = initialize_required_dicts(items)

    #SWL: for every item (a.k.a. phone call or text message) in items, put those messages into stats
    for i in range(0, len(items)):
        current_item = items[i]
        previous_item = None if i == 0 else items[i-1]
        next_item = None if i == len(items) - 1 else items[i+1]  # if current_item is last item then next_item is None

        if is_a_message(current_item):
            if previous_item and is_a_message(previous_item):
                previous_message = process_one_item(previous_item)
            else:
                previous_message = None
            current_message = add_response_info_to_item(process_one_item(current_item), previous_message)
            fill_a_message_into_stats(current_message=current_message,
                                      previous_item=previous_item,
                                      next_item=next_item,
                                      stats=stats,
                                      values_history=values_history,
                                      dict_of_list__of_item_records=dict_of_list__of_item_records)
        else:  # item is a phone call
            current_call = add_response_info_to_item(process_one_item(current_item), previous_item)
            fill_a_phone_call_into_stats(current_call=current_call,
                                         stats=stats,
                                         previous_item=previous_item,
                                         next_item=next_item,
                                         values_history=values_history,
                                         dict_of_list__of_item_records=dict_of_list__of_item_records)

    #SWL: after items are put into stats, now calculate the scores
            
    if cached_values_for_all: #SWL: if there is already cached values for all (i.e. if already initialized)
        return calculate_response(stats=stats, values_history=values_history, addresses=addresses,
                                  total_for_all=cached_values_for_all.total,
                                  engagement_for_all=cached_values_for_all.engagement,
                                  interest_for_all=cached_values_for_all.interest), cached_values_for_all
                                  
    else: #SWL:  if there isn't already a cached value for all (i.e. when you want to calculate the score for all)
    
        # SWL: take steps to set 'all' values first before calculating score
        stats['all'] = calculate_stats_all(stats)
        tmp_dict_for_all = stats_compute.compute_response(stats['all'], p_them=THEM, p_me=ME, full_data=True)
        total_for_all = tmp_dict_for_all['total']
        engagement_for_all = tmp_dict_for_all['engagement']
        interest_for_all = tmp_dict_for_all['interest']
        tmp_cached_values_for_all = CachedValues(total=total_for_all, engagement=engagement_for_all, interest=interest_for_all)

        #SWL: then calculate the score
        return calculate_response(stats=stats, values_history=values_history, addresses=addresses,
                                  total_for_all=total_for_all,
                                  engagement_for_all=engagement_for_all,
                                  interest_for_all=interest_for_all), tmp_cached_values_for_all

