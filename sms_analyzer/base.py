# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import sys

from django.conf import settings
from django.http import Http404, JsonResponse
from rest_framework.generics import GenericAPIView
from rest_framework.exceptions import APIException

from sms_analyzer import exceptions as api_exceptions
from sms_analyzer.validators import Invalid

logger = logging.getLogger(__name__)


class AnonymousApiView(GenericAPIView):
    # A list of forms for the current view. Format:
    # {b'GET': Form, b'POST': [Form1, Form2], ...}
    form_class = None

    def initial(self, request, *args, **kwargs):
        logger.info(
            'New request: %s %s://%s%s',
            request.method,
            request.scheme,
            request.get_host(),
            request.get_full_path(),
        )
        super(AnonymousApiView, self).initial(request, *args, **kwargs)
        self.validate_forms()

    def validate_forms(self):
        """
        Check all the data according to forms set,
        put all the validated data to `self.form_values`.
        """
        self.form_values = {}

        if not self.form_class:
            return

        form_class = self.form_class.get(self.request.method.upper())

        if not form_class:
            return

        logger.info('Processing form %s', form_class)

        if form_class.source == 'GET':
            args = self.request.GET
        elif form_class.source == 'POST':
            args = self.request.POST
            args.update(self.request.FILES)
        else:  # pragma: no cover
            raise NotImplemented(
                'Unexpected form data source: %s' % form_class.source,
            )

        logger.debug('Input args: %s', args)

        try:
            self.form_values.update(
                form_class().to_python(args),
            )
        except Invalid as ex:
            raise api_exceptions.ValidationFailedError(
                ex,
                detail='Input args validation failed. Check your request.',
            )

        logger.info('Form have been processed successfully.')

    def get_object(self):
        """
        When original `get_object` raises Http404,
        let's try to convert it into ApiError with valid error code.
        """
        logger.debug('Querying object from db...')

        try:
            return super(AnonymousApiView, self).get_object()
        except Http404:
            queryset = self.get_queryset()
            raise api_exceptions.NotFoundError(queryset.model.__name__)

    def apply_ordering(self, queryset):
        if self.form_values['order']:
            return queryset.order_by(self.form_values['order'])

        # Normally you never get here, because `order` is passed either in args,
        # or default value applied. But it's not an error, if it happens.
        return queryset  # pragma: no cover

    def apply_range_filters(self, query, field_names):
        for name in field_names:
            filter_values = self.form_values[name]

            if filter_values is None:
                continue
            elif isinstance(filter_values, int):
                filter_dict = {name: filter_values}
            else:
                filter_dict = {}
                min_val, max_val = filter_values
                if min_val is not None:
                    filter_dict['%s__gte' % name] = min_val

                if max_val is not None:
                    filter_dict['%s__lte' % name] = max_val

            query = query.filter(**filter_dict)

        return query


def custom_error_handler(status_code, error_code):
    def handler(request, **kwargs):
        exc = sys.exc_info()[1]
        log_method = logging.info if isinstance(exc, APIException) else logging.exception

        log_method(
            'Error response. status_code=%s, error_code=%s',
            status_code,
            error_code,
        )
        return JsonResponse(
            {'status': 'error', 'error_code': error_code},
            status=status_code,
        )

    return handler


custom_400_handler = custom_error_handler(400, 'bad_request')
custom_403_handler = custom_error_handler(403, 'permission_denied')
custom_404_handler = custom_error_handler(404, 'not_found')
custom_500_handler = custom_error_handler(500, 'internal')
