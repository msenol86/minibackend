# -*- coding: utf-8 -*-

from __future__ import unicode_literals, division, print_function, absolute_import

import os
from concurrent import futures
from sms_analyzer import models
from io import BytesIO
import xlsxwriter
import string
from django.utils import timezone
from django.http import HttpResponse
import logging
from . utils import get_user
from . stats_view import SMSLogShowView
from rest_framework.decorators import api_view, permission_classes, renderer_classes, authentication_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer, HTMLFormRenderer
from rest_framework.authentication import SessionAuthentication
from django.core.mail import send_mail, EmailMessage
from threading import Thread
import smtplib
from django.shortcuts import render

from django.core.exceptions import PermissionDenied


def superuser_only(function):

    def _inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return function(request, *args, **kwargs)

    return _inner


logger = logging.getLogger(__name__)

COLUMNS = ['user_id', 'contact_address', 'owner', 'relationship', 'text_messages_count', 'phone_calls_count',
           'response_score', 'repeat_percentage', 'question_marks_percentage', 'exclamations_percentage',
           'emojis_percentage', 'links_percentage', 'phone_calls_percentage', 'picture_count_percentage',
           'initiation_percentage', 'engagement_percentage',
           'average_message_length',
           'span_of_data', 'texts_per_day','calls_per_day', 'correspondence_per_day',
           'days_of_texting', 'days_of_calling', 'days_of_correspondence',
           'typed_character_count', 'repeat_texts_count', 'question_marks_count',
           'exclamations_count',
           'emojis_count', 'links_count', 'pictures_count', 'initiation_count', 'engagement_count',
           'age', 'gender', 'location', 'feedback',
           'first_day_as_date', 'last_day_as_date', 'first_day',
           'last_day']
READONLY_COLUMNS = ('first_day', 'first_day_as_date', 'last_day', 'last_day_as_date', 'span_of_data',
                    'texts_per_day', 'calls_per_day', 'correspondence_per_day',
                    'typed_character_count', 'emojis_percentage', 'links_percentage',
                    'question_marks_percentage',
                    'exclamations_percentage', 'phone_calls_percentage', 'picture_count_percentage',
                    'initiation_percentage', 'engagement_percentage',
                    'repeat_texts_count')


def postpone(function):
    def decorator(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator


def _generate_worksheet_and_workbook_and_output():
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()
    for a_letter in string.ascii_uppercase:
        worksheet.set_column(a_letter+':'+a_letter, width=20)

    for i, tmp_column in enumerate(COLUMNS):
        worksheet.write(0, i, tmp_column)
    return worksheet, workbook, output


def _write_worksheet_to_file_and_return_response(p_workbook, p_output, p_filename):
    p_workbook.close()
    ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    a_response = HttpResponse(p_output.getvalue(), content_type=ct)
    a_response['Content-Disposition'] = p_filename
    return a_response


def _write_worksheet_to_file_and_send_email(p_workbook, p_output, p_filename, request):
    p_workbook.close()
    email_address = request.user.email
    an_email = EmailMessage("Excel File", "Excel File", "info@crushhapp.com", [email_address])
    ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    file_content = p_output.getvalue()
    with open(os.path.join("excel", p_filename), "w") as tmp_f:
        tmp_f.write(file_content)
    an_email.attach(p_filename, p_output.getvalue(), ct)
    an_email.send()


def _get_contacts(relationship, a_user):
    if relationship:
        if relationship in ("crush", "family", "friend"):
            tmp_contacts = a_user.get_contacts_analyzed()
            tmp_contacts = [a_contact for a_contact in tmp_contacts if a_contact.relation_status == relationship]
        else:
            tmp_contacts = a_user.get_contacts_analyzed()
    else:
        tmp_contacts = a_user.get_contacts_analyzed()

    return tmp_contacts


def _get_contacts_with_stat(relationship, tmp_user):
    tmp_contacts = _get_contacts(relationship=relationship, a_user=tmp_user)
    return [a_contact for a_contact in tmp_contacts if models.ContactStats.objects.filter(contact=a_contact, is_for_me=True).exists() and models.ContactStats.objects.filter(contact=a_contact, is_for_me=False).exists()]


def _write_into_columns(worksheet, row_number_for_me, row_number_for_them, tmp_user, a_contact,
                        contact_stats_for_me, contact_stats_for_them):
    for i, tmp_column in enumerate(COLUMNS):
        if tmp_column == 'user_id':
            worksheet.write(row_number_for_me, i, str(tmp_user.user_id))
            worksheet.write(row_number_for_them, i, str(tmp_user.user_id))
        elif tmp_column == 'contact_address':
            worksheet.write(row_number_for_me, i, contact_stats_for_me.contact.contact_number)
            worksheet.write(row_number_for_them, i, contact_stats_for_them.contact.contact_number)
        elif tmp_column == 'owner':
            worksheet.write(row_number_for_me, i, 'ME')
            worksheet.write(row_number_for_them, i, 'THEM')
        elif tmp_column == 'relationship':
            worksheet.write(row_number_for_me, i, a_contact.relation_status)
            worksheet.write(row_number_for_them, i, a_contact.relation_status)
        elif tmp_column == "age":
            worksheet.write(row_number_for_me, i, tmp_user.age_min.strip() if tmp_user.age_min else None)
            worksheet.write(row_number_for_them, i, a_contact.age)
        elif tmp_column == "gender":
            worksheet.write(row_number_for_me, i, tmp_user.gender)
            worksheet.write(row_number_for_them, i, a_contact.gender)
        elif tmp_column == "location":
            worksheet.write(row_number_for_me, i, tmp_user.location)
            worksheet.write(row_number_for_them, i, a_contact.location)
        elif tmp_column == "feedback":
            worksheet.write(row_number_for_me, i, a_contact.feedback_me)
            worksheet.write(row_number_for_them, i, a_contact.feedback_them)
        elif tmp_column in READONLY_COLUMNS:
            worksheet.write(row_number_for_me, i, getattr(contact_stats_for_me, tmp_column)())
            worksheet.write(row_number_for_them, i, getattr(contact_stats_for_them, tmp_column)())
        else:
            worksheet.write(row_number_for_me, i, getattr(contact_stats_for_me, tmp_column))
            worksheet.write(row_number_for_them, i, getattr(contact_stats_for_them, tmp_column))


@postpone
def cpu_heavy_code_for_excel_all_users(relationship, request):
    worksheet, workbook, output = _generate_worksheet_and_workbook_and_output()

    start = 1

    for tmp_user in models.User.objects.all():
    
#    for tmp_user in models.User.objects.filter(id__in=[1, 2, 3, 4, 3789, 3790]):   
        contacts_with_stats = _get_contacts_with_stat(relationship=relationship, tmp_user=get_user(tmp_user))

        
        for j, a_contact in enumerate(contacts_with_stats, start=start):
            contact_stats_for_me = models.ContactStats.objects.get(contact=a_contact, is_for_me=True)
            contact_stats_for_them = models.ContactStats.objects.get(contact=a_contact, is_for_me=False)

            row_number_for_me = (j * 2) - 1
            row_number_for_them = j * 2
            start += 1

            _write_into_columns(worksheet=worksheet, row_number_for_me=row_number_for_me,
                                row_number_for_them=row_number_for_them, tmp_user=get_user(tmp_user),
                                a_contact=a_contact,
                                contact_stats_for_me=contact_stats_for_me,
                                contact_stats_for_them=contact_stats_for_them)

    logger.info("************** Got all the sql *******")
    
    tmp_filename = 'all_user_stats_' + str(relationship) + str(timezone.datetime.today().date()) + ".xlsx"
    
    logger.info("******** tmp_filename = : " + tmp_filename)
    _write_worksheet_to_file_and_send_email(workbook,output,tmp_filename,request)


@postpone
def cpu_heavy_code_for_excel(relationship, user_id, request):
    worksheet, workbook, output = _generate_worksheet_and_workbook_and_output()

    contacts_with_stats = _get_contacts_with_stat(relationship=relationship, tmp_user=get_user(user_id))

    for j, a_contact in enumerate(contacts_with_stats, start=1):
        contact_stats_for_me = models.ContactStats.objects.get(contact=a_contact, is_for_me=True)
        contact_stats_for_them = models.ContactStats.objects.get(contact=a_contact, is_for_me=False)

        row_number_for_me = (j * 2) - 1
        row_number_for_them = j * 2

        _write_into_columns(worksheet=worksheet, row_number_for_me=row_number_for_me,
                            row_number_for_them=row_number_for_them, tmp_user=get_user(user_id), a_contact=a_contact,
                            contact_stats_for_me=contact_stats_for_me, contact_stats_for_them=contact_stats_for_them)

    tmp_filename = str(user_id) + "_stats_" + str(timezone.datetime.today().date()) + ".xlsx"
    _write_worksheet_to_file_and_send_email(workbook, output, tmp_filename, request)


@superuser_only
def get_stats_as_excel_all_users(request, relationship=None):
    cpu_heavy_code_for_excel_all_users(relationship, request=request)
    return HttpResponse("Task Started. Excel File will be sent to " + request.user.email)


@superuser_only
def get_stats_as_excel(request, user_id, relationship=None):
    cpu_heavy_code_for_excel(relationship, user_id, request)
    return HttpResponse("Task Started. Excel File will be sent to " + request.user.email)


@api_view(['GET', ])
# @permission_classes([IsAdminUser, ])
# @authentication_classes([SessionAuthentication, ])
@superuser_only
@renderer_classes([JSONRenderer, ])
def analyze_all_contacts(request, user_id_or_user):
    """
    Analyze all sms messages of user and create contactstats for all contacts (even the contacts not picked for analyze by end user)
    """

    analyzer_all_contacts_helper(user_id_or_user, request)
    return HttpResponse("Task Started on Background")


@api_view(['GET', ])
@superuser_only
@renderer_classes((HTMLFormRenderer, ))
def analyze_all_contact_for_all_users_confirm(request):
    return render(request, 'sms_analyzer/confirm.html')


@api_view(['POST', ])
@superuser_only
@renderer_classes([JSONRenderer, ])
def analyze_all_contact_for_all_users_hidden(request):
    is_confirmed = request.POST['is_confirmed']
    if is_confirmed:
        if bool(is_confirmed):
            analyze_all_contact_for_all_users_helper(request)
            return HttpResponse("Task Started on Background")
        else:
            return HttpResponse("You cannot call this url directly")
    else:
        return HttpResponse("You cannot call this url directly")




@postpone
def analyze_all_contact_for_all_users_helper(request=None):
    for tmp_user in models.User.objects.all():
        addresses = tmp_user.get_unique_contacts_addresses()

        for an_address in addresses:
            a_contact, is_created = models.Contact.objects.get_or_create(user=tmp_user,
                                                                         contact_number=an_address)

        smslogshowview = SMSLogShowView()
        smslogshowview._format_stats(user=tmp_user, addresses=addresses, ignore_cache=True)
    if request:
        email_address = request.user.email
        an_email = EmailMessage("Background Task Report", "analyze_all_contact_for_all_users() task finished", "info@crushhapp.com",
                                [email_address])
        an_email.send()


@postpone
def analyzer_all_contacts_helper(user_id_or_user, request=None):
    tmp_user = get_user(user_id_or_user)
    addresses = tmp_user.get_unique_contacts_addresses()

    for an_address in addresses:
        a_contact, is_created = models.Contact.objects.get_or_create(user=tmp_user,
                                                                     contact_number=an_address)

    smslogshowview = SMSLogShowView()
    smslogshowview._format_stats(user=tmp_user, addresses=addresses, ignore_cache=True)
    if request:
        email_address = request.user.email
        an_email = EmailMessage("Background Task Report", "analyze_all_contacts() task finished", "info@crushhapp.com",
                                [email_address])
        an_email.send()


