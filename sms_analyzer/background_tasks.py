import multiprocessing
from multiprocessing.pool import ThreadPool
import models
from random import shuffle
from wrappers import format_stats_view_wrapper_direct
import logging
from django import db

logger = logging.getLogger(__name__)


def my_task():

    all_users = models.User.objects.all()
    all_users_id = [tmp_user.user_id for tmp_user in all_users]
    shuffle(all_users_id)
    all_users_id = all_users_id[:100]
    # all_users_id = [u'0995362316',
    #                 u'4079538351',
    #                 u'8155494496',
    #                 u'9271321382',
    #                 u'8064333857',
    #                 u'3104894358',
    #                 u'6783538777',
    #                 u'7146180146',
    #                 u'3236754175',
    #                 u'2672126637',
    #                 u'7085419949',
    #                 u'7609178546',
    #                 u'7734547958',
    #                 u'7184500609',
    #                 u'7739634338',
    #                 u'8184168358',
    #                 u'1983062875',
    #                 u'3239461384',
    #                 u'4045878210',
    #                 u'5624179816',
    #                 u'7734503951',
    #                 u'8189296745',
    #                 u'9173875923',
    #                 u'3154369342',
    #                 u'3475988474',
    #                 u'9819645466',
    #                 u'5737896207',
    #                 u'4043996559',
    #                 u'3474513681',
    #                 u'2016865878']
    logger.info("number of users to recache: " + str(len(all_users_id)))
    db.connections.close_all()
    # p = multiprocessing.Pool(processes=cpu_core_count)
    p = ThreadPool(processes=30)
    result_list = p.map(format_stats_view_wrapper_direct, all_users_id)
    logger.info(str(result_list.count(False)) + " items failed")
    p.close()
    db.connections.close_all()
