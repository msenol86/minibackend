# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division

from django.utils.encoding import python_2_unicode_compatible
import uuid
from django.utils import timezone
import datetime
from django.db import models
from django.contrib.auth.models import AbstractBaseUser


def convert_unixtime_in_ms_to_datetime(unixtime_in_miliseconds):
    return datetime.datetime.fromtimestamp(unixtime_in_miliseconds / 1000)

#assert convert_unixtime_in_ms_to_datetime(1477656078589) == datetime.datetime(2016, 10, 28, 8, 1, 18, 589000)


class SMSLog(models.Model):
    user = models.ForeignKey('User', null=False)
    address = models.CharField(db_index=True, max_length=255, null=False, blank=False)
    name = models.CharField(max_length=255, null=False, blank=True)
    type = models.CharField(db_index=True, max_length=255, null=False, blank=False)
    datetime = models.DateTimeField(db_index=True, null=False, blank=False)
    body = models.CharField(max_length=65535, null=False, blank=True)
    picture_count = models.IntegerField(null=False, blank=False, default=0)
    timezone_offset = models.IntegerField(null=True, blank=True)
    source = models.CharField(max_length=255, null=True, blank=True, choices=(('sms', 'sms'),
                                                                              ('mms', 'mms'),
                                                                              ('phone_call', 'phone_call'),
                                                                              ('whatsapp_web', 'whatsapp_web'),
                                                                              ('whatsapp_email', 'whatsapp_email')))
    upload_type = models.CharField(max_length=255, null=True, blank=True, choices=(('initial', 'initial'),
                                                                                   ('slot_notification', 'slot_notification'),
                                                                                   ('analyze_button', 'analyze_button')))
    g_polarity = models.FloatField(null=True, blank=True)
    g_magnitude = models.FloatField(null=True, blank=True)
    w_mixed = models.NullBooleanField(null=True, blank=True)
    w_score = models.FloatField(null=True, blank=True)
    w_type = models.CharField(max_length=255, null=True, blank=True, choices=(('positive', 'positive'),
                                                                              ('negative', 'negative'),
                                                                              ('neutral', 'neutral')))
    keyword_count = models.CharField(max_length=255, null=True, blank=True)
    
    spam = models.BooleanField(default=False) #Is this message likely to be spam? (More than 5x incoming over outgoing)
    outlier = models.BooleanField(default=False) #Is this message an outlier
    
    # These five count values below do not ever change. So we save them once in this table
    # when the record is first analyzed to find spam, outliers or to populate AggregateStatsCache.
    # After that they can just be looked up here easily to save the processing time of 
    # continuously deriving these values from the body of the text.
    body_length_count = models.IntegerField(null=True, blank=True)
    emojis_count = models.IntegerField(null=True, blank=True)
    links_count = models.IntegerField(null=True, blank=True)
    exclamations_count = models.IntegerField(blank=True, null=True)
    question_marks_count = models.IntegerField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class User(AbstractBaseUser):
    USERNAME_FIELD = 'user_id'
    DEFAULT_REFERRAL_COUNT = 0
    user_id = models.CharField(max_length=255, null=False, blank=False, unique=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    gender = models.CharField(max_length=255, choices=(('male', 'male'), ('female', 'female')), blank=True, null=True)
    birthday = models.CharField(max_length=255, null=True, blank=True)
    registration_type = models.CharField(max_length=255, blank=True, null=True, choices=(('facebook', 'facebook'), ('manual', 'manual')))
    location = models.CharField(max_length=255, blank=True, null=True)
    timestamp = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    age_min = models.CharField(max_length=255, blank=True, null=True)
    referrer = models.CharField(max_length=255, blank=True, null=True)
    referral_code = models.CharField(max_length=255, blank=True, null=True)
    manual_referral_count = models.IntegerField(blank=True, null=True, default=DEFAULT_REFERRAL_COUNT, help_text="Referral Count Bonus")
    version_code = models.IntegerField(blank=True, null=True, default=20)
    alert_msg = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        default_permissions = ('add', 'change', 'delete', 'view')

    def get_contacts_manually_analyzed(self):
        return Contact.objects.filter(user=self, relation_status__in=("friend", "family", "crush"))

    def get_contacts_manually_analyzed_count(self):
        return self.get_contacts_manually_analyzed().count()

    def get_contacts_analyzed(self):
        return Contact.objects.filter(user=self)

    def get_contacts_analyzed_count(self):
        return self.get_contacts_analyzed().count()

    def get_unique_contacts_addresses(self):
        return SMSLog.objects.filter(user=self).values_list('address', flat=True).distinct()

    def get_unique_contacts_count(self):
        return self.get_unique_contacts_addresses().count()

    def get_message_count(self):
        return SMSLog.objects.filter(user=self).count()

    def get_referral_count_used_by_app(self):
        if self.referral_code and self.referral_code != "":
            return self.get_real_referral_count() + self.manual_referral_count
        else:
            return self.manual_referral_count

    def get_referral_count(self):
        return self.get_referral_count_used_by_app()

    def get_real_referral_count(self):
        if self.referral_code and self.referral_code != "":
            return User.objects.filter(referrer=self.referral_code).exclude(referral_code=self.referral_code).count()
        else:
            return 0

    def get_referrers(self):
        if self.referral_code and self.referral_code != "":
            return User.objects.filter(referrer=self.referral_code)
        else:
            None

    def get_response_info_after_register(self):
        tmp_response = dict()
        tmp_response['user_id'] = self.user_id
        tmp_response['name'] = self.name
        tmp_response['email'] = self.email
        tmp_response['gender'] = self.gender
        tmp_response['birthday'] = self.birthday
        tmp_response['location'] = self.location
        tmp_response['registration_type'] = self.registration_type
        tmp_response['referrer'] = self.referrer
        tmp_response['get_referral_count'] = self.get_referral_count()
        return tmp_response

    def get_signup_datetime(self):
        if self.timestamp and len(self.timestamp) == 13 and str(self.timestamp).isdigit():
            return convert_unixtime_in_ms_to_datetime(int(self.timestamp))
        else:
            return None

    def update_last_login_time_and_save(self):
        self.last_login = timezone.now()
        self.save()


@python_2_unicode_compatible
class UserStatsRecord(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, null=False, blank=False)
    is_for_me = models.BooleanField(null=False, blank=False)
    text_messages_count = models.IntegerField(null=True, blank=True)
    response_score = models.FloatField(null=True, blank=True)
    repeat_percentage = models.FloatField(null=True, blank=True)
    average_message_length = models.IntegerField(blank=True, null=True)
    phone_calls_count = models.IntegerField(null=True, blank=True)
    pictures_count = models.IntegerField(null=True, blank=True)
    emojis_count = models.IntegerField(null=True, blank=True)
    links_count = models.IntegerField(null=True, blank=True)
    exclamations_count = models.IntegerField(blank=True, null=True)
    question_marks_count = models.IntegerField(blank=True, null=True)

    # data_for_all structure:
    # [{'calls': {'me': 491.0, 'ratio': 1.3128061581525543, 'them': 938.0},
    #   'emojis': {'me': 754.0, 'ratio': 1.468571401200456, 'them': 2632.0},
    #   'engagement': 1.092512573454271,
    #   'exclamations': {'me': 3900.0, 'ratio': 1.1970931674453558, 'them': 7345.0},
    #   'history': [],
    #   'id': 'all',
    #   'interest': 1.2255897401402462,
    #   'length': {'me': 24.79492389997097,
    #              'ratio': 1.0063605993035918,
    #              'them': 25.112364161659936},
    #   'links': {'me': 158.0, 'ratio': 1.537780918665469, 'them': 664.0},
    #   'questions': {'me': 9840.0, 'ratio': 0.88536496615504, 'them': 9873.0},
    #   'repeat': {'me': 0.5374486791357359,
    #              'ratio': 1.0818155637855993,
    #              'them': 0.6332282740733445},
    #   'response': {'me': 0, 'ratio': 1, 'them': 0},
    #   'response_score': {'me': 0, 'ratio': 1, 'them': 0},
    #   'responsiveness': 1.0,
    #   'texts': {'me': 48226.0, 'ratio': 1.128361237950279, 'them': 60918.0},
    #   'total': 1.0951740654466349}]

    def typed_character_count(self):
        if self.average_message_length and self.text_messages_count:
            return self.average_message_length * self.text_messages_count

    def texts_per_day(self): 
        if self.text_messages_count and self.user.userstatscache.span_of_texts():
            return self.text_messages_count / self.user.userstatscache.span_of_texts()

    def calls_per_day(self): 
        if self.phone_calls_count and self.user.userstatscache.span_of_calls():
            return self.phone_calls_count / self.user.userstatscache.span_of_calls() 

    def correspondence_per_day(self):
        if (self.phone_calls_count or self.text_messages_count) and self.user.userstatscache.span_of_data():
            return (self.phone_calls_count + self.text_messages_count) / self.user.userstatscache.span_of_data()
      

    def emojis_percentage(self):
        if self.emojis_count and self.text_messages_count:
            return self.emojis_count / self.text_messages_count

    def links_percentage(self):
        if self.links_count and self.text_messages_count:
            return self.links_count / self.text_messages_count

    def question_marks_percentage(self):
        if self.question_marks_count and self.text_messages_count:
            return self.question_marks_count / self.text_messages_count

    def exclamations_percentage(self):
        if self.exclamations_count and self.text_messages_count:
            return self.exclamations_count / self.text_messages_count

    def phone_calls_percentage(self):
        if self.phone_calls_count and self.text_messages_count:
            return self.phone_calls_count / self.text_messages_count

    def picture_count_percentage(self):
        if self.pictures_count and self.text_messages_count:
            return self.pictures_count / self.text_messages_count

    def repeat_texts_count(self):
        if self.repeat_percentage and self.text_messages_count:
            return self.repeat_percentage * self.text_messages_count

    def __str__(self):
        return str(self.user)

    class Meta:
        unique_together = (("user", "is_for_me"),)


class UserStatsCache(models.Model):
    user = models.OneToOneField(User, null=False, blank=False)
    score_for_all = models.FloatField(null=True, blank=True, default=0.0, help_text="Unadjusted for personal texting behavior")
    engagement_for_all = models.FloatField(null=True, blank=True, default=0.0, help_text="Unadjusted for personal texting behavior")
    interest_for_all = models.FloatField(null=True, blank=True, default=0.0, help_text="Unadjusted for personal texting behavior")
    message_count_before_cache = models.IntegerField(null=True, blank=True, default=-1)
    first_day = models.IntegerField(blank=True, null=True)
    first_day_as_date = models.DateField(blank=True, null=True)
    last_day = models.IntegerField(blank=True, null=True)
    last_day_as_date = models.DateField(blank=True, null=True)
    first_phone_call =  models.IntegerField(blank=True, null=True) 
    first_phone_call_as_date = models.DateField(blank=True, null=True)
    last_phone_call =  models.IntegerField(blank=True, null=True) 
    last_phone_call_as_date = models.DateField(blank=True, null=True)   
    first_text =  models.IntegerField(blank=True, null=True) 
    first_text_as_date = models.DateField(blank=True, null=True)
    last_text =  models.IntegerField(blank=True, null=True) 
    last_text_as_date = models.DateField(blank=True, null=True)       
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def span_of_data(self):
        if self.first_day and self.last_day:
            return self.last_day - self.first_day
            
    def span_of_calls(self):
        if self.first_phone_call and self.last_phone_call:
            return self.last_phone_call - self.first_phone_call

    def span_of_texts(self):
        if self.first_text and self.last_text:
            return self.last_text - self.first_text
         
    def is_cache_up_to_date(self, p_message_count):
        return p_message_count == self.message_count_before_cache

    def cache_score_and_save(self, p_cached_values_for_all, p_message_count, first_day_as_date, first_day, 
                             last_day_as_date, last_day, first_phone_call_as_date, first_phone_call,
                             last_phone_call_as_date, last_phone_call, first_text_as_date, first_text,
                             last_text_as_date, last_text):
        
        self.score_for_all = float(p_cached_values_for_all.total)
        self.engagement_for_all = float(p_cached_values_for_all.engagement)
        self.interest_for_all = float(p_cached_values_for_all.interest)
        self.first_day_as_date = first_day_as_date
        self.first_day = first_day
        self.last_day_as_date = last_day_as_date
        self.last_day = last_day
        self.first_phone_call_as_date = first_phone_call_as_date
        self.first_phone_call = first_phone_call
        self.last_phone_call_as_date = last_phone_call_as_date
        self.last_phone_call = last_phone_call  
        self.first_text_as_date = first_text_as_date
        self.first_text = first_text
        self.last_text_as_date = last_text_as_date
        self.last_text = last_text  
        self.message_count_before_cache = p_message_count
        self.save()


class ContactStats(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    contact = models.ForeignKey('Contact', null=False, blank=False, related_name='contact')
    is_for_me = models.BooleanField(null=False, blank=False)
    text_messages_count = models.IntegerField(null=True, blank=True)
    phone_calls_count = models.IntegerField(null=True, blank=True)
    pictures_count = models.IntegerField(null=True, blank=True)
    emojis_count = models.IntegerField(null=True, blank=True)
    links_count = models.IntegerField(null=True, blank=True)
    exclamations_count = models.IntegerField(blank=True, null=True)
    initiation_count = models.IntegerField(blank=True, null=True)
    engagement_count = models.IntegerField(blank=True, null=True)
    question_marks_count = models.IntegerField(blank=True, null=True)
    response_score = models.FloatField(null=True, blank=True)
    repeat_percentage = models.FloatField(null=True, blank=True)
    average_message_length = models.IntegerField(blank=True, null=True)
    days_of_texting = models.IntegerField(blank=True, null=True)
    days_of_calling = models.IntegerField(blank=True, null=True)
    days_of_correspondence = models.IntegerField(blank=True, null=True)
    
    def first_day(self):
        return self.contact.first_day

    def first_day_as_date(self):
        return self.contact.first_day_as_date

    def last_day(self):
        return self.contact.last_day

    def last_day_as_date(self):
        return self.contact.last_day_as_date

    def span_of_data(self):
        return self.contact.span_of_data()

    def typed_character_count(self):
        if self.average_message_length and self.text_messages_count:
            return self.average_message_length * self.text_messages_count

    def texts_per_day(self):
        if self.text_messages_count and self.days_of_texting:
            return self.text_messages_count / self.days_of_texting
            
    def calls_per_day(self):
        if self.phone_calls_count and self.days_of_calling:
            return self.phone_calls_count / self.days_of_calling
                    
    def correspondence_per_day(self):
        if (self.phone_calls_count or self.text_messages_count) and self.days_of_correspondence:
            return (self.phone_calls_count + self.text_messages_count) / self.days_of_correspondence
                       
    def emojis_percentage(self):
        if self.emojis_count and self.text_messages_count:
            return self.emojis_count / self.text_messages_count

    def links_percentage(self):
        if self.links_count and self.text_messages_count:
            return self.links_count / self.text_messages_count

    def question_marks_percentage(self):
        if self.question_marks_count and self.text_messages_count:
            return self.question_marks_count / self.text_messages_count

    def exclamations_percentage(self):
        if self.exclamations_count and self.text_messages_count:
            return self.exclamations_count / self.text_messages_count

    def initiation_percentage(self):
        if self.initiation_count and self.text_messages_count:
            return self.initiation_count / self.text_messages_count

    def engagement_percentage(self):
        if self.engagement_count and self.text_messages_count:
            return self.engagement_count / self.text_messages_count

    def phone_calls_percentage(self):
        if self.phone_calls_count and self.text_messages_count:
            return self.phone_calls_count / self.text_messages_count

    def picture_count_percentage(self):
        if self.pictures_count and self.text_messages_count:
            return self.pictures_count / self.text_messages_count

    def repeat_texts_count(self):
        if self.repeat_percentage and self.text_messages_count:
            return self.repeat_percentage * self.text_messages_count

    class Meta:
        unique_together = (("contact", "is_for_me"),)
        verbose_name_plural = "contact_stats"


@python_2_unicode_compatible
class Contact(models.Model):
    user = models.ForeignKey(User, null=False, blank=False)
    contact_number = models.CharField(max_length=255, null=False, blank=False)
    relation_status = models.CharField(max_length=255, blank=False, null=False, choices=(('crush', 'crush'),
                                                                                         ('family', 'family'),
                                                                                         ('friend', 'friend'),
                                                                                         ('unspecified', 'unspecified'),
                                                                                         ('spam', 'spam')), default='unspecified')
    gender = models.CharField(max_length=255, choices=(('male', 'male'), ('female', 'female')), blank=True, null=True)
    age = models.IntegerField(null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True)
    feedback_me = models.FloatField(null=True, blank=True)
    feedback_them = models.FloatField(null=True, blank=True)
    vote = models.IntegerField(default = 0, null=True, blank=True)
    vote_timestamp = models.CharField(max_length=255, blank=True, null=True)
    score = models.FloatField(null=True, blank=True)
    first_day = models.IntegerField(blank=True, null=True)
    first_day_as_date = models.DateField(blank=True, null=True)
    last_day = models.IntegerField(blank=True, null=True)
    last_day_as_date = models.DateField(blank=True, null=True)
  
            
    def span_of_data(self):
        if self.first_day and self.last_day:
            return self.last_day - self.first_day        

    class Meta:
        unique_together = (("user", "contact_number"),)

    def get_contact_stats_for_me(self):
        return ContactStats.objects.get(contact=self, is_for_me=True)

    def get_contact_stats_for_them(self):
        return ContactStats.objects.get(contact=self, is_for_me=False)

    def get_vote_datetime(self):
        if self.vote_timestamp and len(self.vote_timestamp) == 13 and str(self.vote_timestamp).isdigit():
            return convert_unixtime_in_ms_to_datetime(int(self.vote_timestamp))
        else:
            return None

    def __str__(self):
        return str(self.user) + ", " + str(self.contact_number)

    
class AppFeedback(models.Model):
    phone_number = models.CharField(max_length=50, null=False, blank=False)
    content = models.TextField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)


class Referral(models.Model):
    phone_number = models.CharField(max_length=50, null=False, blank=False)
    referree = models.EmailField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

