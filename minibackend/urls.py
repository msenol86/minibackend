"""minibackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from sms_analyzer.views import test
from sms_analyzer import stats_view
from sms_analyzer import wrappers

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'test/', test),
    url(r'recache/(?P<user_id>[a-zA-Z0-9_\-\.]+)$', wrappers.format_stats_view_wrapper),
    #url(r'^api/sms_log/\s*(?P<user_id>[a-zA-Z0-9_\-\.]+)\.(?P<a_format>json|csv|xls|stats)$', stats_view.SMSLogShowView.as_view()),
]
